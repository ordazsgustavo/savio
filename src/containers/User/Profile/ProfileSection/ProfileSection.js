import React from 'react';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import { Link } from 'react-router-dom';

import classes from './ProfileSection.css';

const profileSection = (props) => {
  const icon = props.icon ? <span className={classes.Icon}><Link to="/accounts"><FontAwesomeIcon icon={props.icon} /></Link></span> : null;
  return (
    <div className={classes.ProfileSection}>
      <div className={classes.Title}>
        <h3>{props.title}</h3>
        {icon}
      </div>
      <div className={classes.Content}>
        {props.children}
      </div>
    </div>
  )
}
export default profileSection