import React from 'react';

import classes from './ForgotPassword.css';
import Aux from '../../../hoc/Aux/Aux';
import Header from '../../../components/UI/Header/Header';
import Panel from '../../../components/UI/Panel/Panel';
import Form from '../../../components/Form/Form';

const forgotPassword = () => (
  <Aux>
    <Header>Olvidé mi contraseña</Header>
    <div className={classes.ForgotPassword}>
      <Panel>
        <p>Para recuperar tu contraseña ingresa tu correo electrónico y te enviaremos un email de verificació</p>
        <p>Si no recibes un correo electrónico tas unos minutos, puedes solicitar el envio nuevamente.</p>
        <Form type="ForgotPassword" submitButton="Recuperar contraseña" />
      </Panel>
    </div>
  </Aux>
)

export default forgotPassword;