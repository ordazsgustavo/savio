const forgotPassword = {
  email: { 
    elementType: 'input', 
    elementConfig: {
      type: 'email',
      autoComplete: 'username'
    },
    value: '',
    validation: {
      required: true
    },
    valid: false,
    touched: false,
    label: 'Correo',
    errorMessage: ''
  }
}

export default forgotPassword