import React from 'react';

import classes from './Features.css';
import SmallCard from '../../../components/SmallCard/SmallCard';

const features = () => (
  <div className={classes.Features}>
    <h2>Por que elegir <span>S</span>AVIO</h2>
    <div className={classes.Wrapper}>
      <SmallCard icon="lock" title="Seguridad">Siéntete libre de usar nuestra web ya que no compartirás tus cuentas. Certificado SSL.</SmallCard>
      <SmallCard icon="dollar-sign" title="Mejor tipo de cambio">Las casas de cambio listadas ofrecerán el tipo de cambio del día y tendrás la potestad de elegir.</SmallCard>
      <SmallCard icon="mobile" title="Desde tu dispositivo">No tienes la necesidad de salir a la calle, tus operaciones las harás desde tu dispositivo.</SmallCard>
      <SmallCard icon="life-ring" title="Desde tu dispositivo">Al realizar tus operaciones tendrás apoyo virtual en tiempo real para tus dudas y/o consultas.</SmallCard>
      <SmallCard icon="rocket" title="Rapidez">No pierdas tiempo saliendo a la calle a buscar tu cambio de dinero, hazlo por SAVIO.</SmallCard>
      <SmallCard icon="thumbs-up" title="Fácil de usar">Hemos buscado que el proceso para realizar tus operaciones sea sencillo y fácil de entender.</SmallCard>
    </div>
  </div>
)

export default features;