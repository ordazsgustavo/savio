const AddAccount = {
  bank: {
    elementType: 'select',
    elementConfig: {
      options: [
        {value: 'BCP', displayValue: 'BCP'},
        {value: 'BBVA', displayValue: 'BBVA Continental'},
        {value: 'Financiero', displayValue: 'Banco Financiero'},
        {value: 'Interbank', displayValue: 'Interbank'},
        {value: 'Scotiabank', displayValue: 'Scotiabank'},
        {value: 'Citi Bank', displayValue: 'Citi Bank'},
        {value: 'Mi Banco', displayValue: 'Mi Banco'}
      ]
    },
    elementClasses: ['Third'],
    value: 'BCP',
    validation: {},
    valid: true,
    errorMessage: ''
  },
  number: {
    elementType: 'input', 
    elementConfig: {
      type: 'tel'
    },
    elementClasses: ['ThreeThird'],
    value: '',
    validation: {
      required: true
    },
    valid: false,
    touched: false,
    label: 'N. de cuenta',
    errorMessage: ''
  },
  currency: {
    elementType: 'select',
    elementConfig: {
      options: [
        {value: 'pen', displayValue: 'PEN'},
        {value: 'usd', displayValue: 'USD'},
        {value: 'eur', displayValue: 'EUR'},
        {value: 'btc', displayValue: 'BTC'}
      ]
    },
    elementClasses: ['Half'],
    value: 'pen',
    validation: {},
    valid: true,
    errorMessage: ''
  },
  owner: {
    elementType: 'select',
    elementConfig: {
      options: [
        {value: 'personal', displayValue: 'Propia'},
        {value: 'third', displayValue: 'De tercero'},
        {value: 'eur', displayValue: 'Empresa'}
      ]
    },
    elementClasses: ['Half'],
    value: 'personal',
    validation: {},
    valid: true,
    errorMessage: ''
  },
  name: {
    elementType: 'input', 
    elementConfig: {
      type: 'text'
    },
    value: '',
    validation: {
      required: true
    },
    valid: false,
    touched: false,
    label: 'Titular',
    errorMessage: ''
  },
  city: {
    elementType: 'select',
    elementConfig: {
      options: [
        {value: 'arequipa', displayValue: 'Arequipa'},
        {value: 'ica', displayValue: 'Ica'},
        {value: 'lima', displayValue: 'Lima'},
        {value: 'la libertad', displayValue: 'La Libertad'},
        {value: 'lambayeque', displayValue: 'Lambayeque'},
        {value: 'piura', displayValue: 'Piura'}
      ]
    },
    value: 'arequipa',
    label: 'Ciudad de apertura',
    validation: {},
    valid: true,
    errorMessage: ''
  }
}

export default AddAccount