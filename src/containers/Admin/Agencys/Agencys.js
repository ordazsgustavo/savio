import React, { Component } from 'react';
import { db } from '../../../lib/firebase';

import Spinner from '../../../components/UI/Spinner/Spinner';
import Table from '../../../components/Table/Table';

class Agencys extends Component {
  state = {
    agencys: [],
    loading: true
  }

  componentWillMount () {
    const agencysRef = db.ref('/agencys');
    agencysRef.on('value', snap => {
      let agencys = [];
      snap.forEach(snap => {
        agencys = [
          {
            ...snap.val(),
            id: snap.key
          },
          ...agencys
        ]
      });

      this.setState({ loading: false, agencys: agencys });
    })
  }

  render () {
    let agencys = null;
    if (this.state.loading) {
      agencys = <Spinner />
    } else {
      this.state.agencys.map(agency => { 
        return agencys = [
          agency.data.ruc,
          agency.data.corporateName,
          agency.data.displayName,
          agency.rating
        ]
      })
    }
    const headers = [
      "RUC", 
      "Razon Social",
      "Nombre Visible",
      "Puntuacion"
    ]
    return (
      <Table headers={headers} rows={agencys} />
    );
  }
}

export default Agencys;