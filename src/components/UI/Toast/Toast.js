import React from 'react';

import classes from './Toast.css';

const toast = (props) => {
  return (
    <div className={classes.Toast}>
      {props.children}
    </div>
  )
}

export default toast;