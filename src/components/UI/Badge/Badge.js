import React from 'react';

import classes from './Badge.css';

const badge = (props) => (
  <div className={classes.Badge} onClick={props.clicked}>
    {props.children}
    <span className={classes.Close} onClick={props.deleted}>&times;</span>
  </div>
)

export default badge;