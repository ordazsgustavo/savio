import React from 'react';
import imagePlaceholder from '../../../assets/images/placeholder.png';

import classes from './Card.css';
import Tag from '../Tag/Tag';

const card = (props) => {
  let headerStyle = [classes.Header];

  if (props.status) {
    headerStyle = [...headerStyle, classes[props.status]]
  }

  let image = null;

  if (props.image) {
    if (props.image === 'placeholder') {
      image = <img src={imagePlaceholder} alt="placeholder"/>;
    } else {
      image = <img src={props.image} alt="placeholder"/>;
    }
  }

  return (
    <div className={classes.Card}>
      <div className={headerStyle.join(' ')}>{props.header}{props.tag ? <Tag type={props.tag.type}>{props.tag.content}</Tag> : null}</div>
      {image}
      <div className={classes.Content}>{props.children}</div>
      {props.button ? <div className={classes.Footer}>{props.button}</div> : null}
    </div>
  )
}

export default card;