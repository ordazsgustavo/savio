import React from 'react';

import classes from './Login.css';
import Aux from '../../../hoc/Aux/Aux';
import Header from '../../../components/UI/Header/Header';
import Panel from '../../../components/UI/Panel/Panel';
import Form from '../../../components/Form/Form';

const login = () => (
  <Aux>
    <Header>Iniciar sesión</Header>
    <div className={classes.Login}>
      <Panel centered>
        <Form type="Login" submitButton="Ingresar" />
      </Panel>
    </div>
  </Aux>
)

export default login;