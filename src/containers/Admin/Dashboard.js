import React, { Component } from 'react';
import { Route } from 'react-router-dom';

import classes from './Dashboard.css';
import Panel from '../../components/UI/Panel/Panel';
import Agencys from './Agencys/Agencys';

class Dashboard extends Component {
  render () {
    return (
      <div className={classes.Dashboard}>
        <div className={classes.Menu}>
          <Panel>
            <div>Menu</div>
            <div>Menu</div>
            <div>Menu</div>
            <div>Menu</div>
            <div>Menu</div>
            <div>Menu</div>
            <div>Menu</div>
          </Panel>
        </div>
        <div className={classes.Content}>
          <Panel>
            <Route path="/dashboard/agencys" component={Agencys} />
          </Panel>
        </div>
      </div>
    );
  }
}

export default Dashboard;