import React, { Component } from 'react';
import { auth, db } from '../../lib/firebase';

import classes from './SavioFlow.css';
import Aux from '../../hoc/Aux/Aux';
import Input from '../../components/UI/Input/Input';
import SavioStepper from './SavioStepper/SavioStepper';
import Button from '../../components/UI/Button/Button';
import SmallCard from '../../components/SmallCard/SmallCard';
import Modal from '../../components/UI/Modal/Modal';
import Form from '../../components/Form/Form';

const STEPS = [
  'change',
  'personalData',
  'targetCurrency',
  'sendingAccount',
  'receivingAccount',
]

class SavioFlow extends Component {
  state = {
    accounts: [],
    change: {
      operation: {
        elementType: 'radio',
        elementConfig: {
          type: 'radio',
          name: 'operation',
          radios: [
            {value: 'want', label: 'Quiero'},
            {value: 'have', label: 'Tengo'}
          ]
        },
        value: 'want',
        validation: {},
        valid: true
      },
      ammount: { 
        elementType: 'input', 
        elementConfig: {
          type: 'tel'
        },
        elementClasses: ['ThreeThird'],
        value: '',
        validation: {
          required: true
        },
        valid: false,
        touched: false,
        label: 'Monto'
      },
      currency1: {
        elementType: 'select',
        elementConfig: {
          options: [
            {value: 'usd', displayValue: 'USD'},
            {value: 'pen', displayValue: 'PEN'},
            {value: 'btc', displayValue: 'BTC'}
          ]
        },
        elementClasses: ['Third'],
        value: 'usd',
        validation: {},
        valid: true
      },
    },
    personalData: {
      ruc: { 
        elementType: 'input', 
        elementConfig: {
          type: 'tel'
        },
        elementClasses: ['Half'],
        value: '',
        validation: {
          required: false,
          minLength: 12,
          maxLength: 12
        },
        valid: true,
        touched: false,
        label: 'RUC',
        errorMessage: ''
      },
      company: { 
        elementType: 'input', 
        elementConfig: {
          type: 'text'
        },
        elementClasses: ['Half'],
        value: '',
        validation: {
          required: false
        },
        valid: true,
        touched: false,
        label: 'Razón social',
        errorMessage: ''
      },
      docType: { 
        elementType: 'select',
        elementConfig: {
          options: [
            {value: 'dni', displayValue: 'DNI'},
            {value: 'carnet extranjeria', displayValue: 'Carnet Extranjería'},
            {value: 'pasaporte', displayValue: 'Pasaporte'}
          ]
        },
        elementClasses: ['Third'],
        value: 'dni',
        validation: {},
        valid: true,
        label: 'ID',
        errorMessage: ''
      },
      document: { 
        elementType: 'input', 
        elementConfig: {
          type: 'text'
        },
        elementClasses: ['ThreeThird'],
        value: '',
        validation: {
          required: true
        },
        valid: false,
        touched: false,
        label: 'Número',
        errorMessage: ''
      },
      name: { 
        elementType: 'input', 
        elementConfig: {
          type: 'text',
          autoComplete: 'given-name'
        },
        elementClasses: ['Half'],
        value: '',
        validation: {
          required: true
        },
        valid: false,
        touched: false,
        label: 'Nombre',
        errorMessage: ''
      },
      lastName: { 
        elementType: 'input', 
        elementConfig: {
          type: 'text',
          autoComplete: 'family-name'
        },
        elementClasses: ['Half'],
        value: '',
        validation: {
          required: true
        },
        valid: false,
        touched: false,
        label: 'Apellido',
        errorMessage: ''
      },
      birthDate: { 
        elementType: 'input', 
        elementConfig: {
          type: 'date'
        },
        value: '',
        validation: {
          required: true
        },
        valid: false,
        touched: false,
        label: 'Fecha de nacimiento',
        errorMessage: ''
      },
      country: { 
        elementType: 'input', 
        elementConfig: {
          type: 'text',
          autoComplete: 'shipping country'
        },
        elementClasses: ['Half'],
        value: 'Perú',
        validation: {
          required: true
        },
        valid: false,
        touched: false,
        label: 'País',
        errorMessage: ''
      },
      department: { 
        elementType: 'select',
        elementConfig: {
          options: [
            {value: 'Amazonas', displayValue: 'Amazonas'},
            {value: 'Ancash', displayValue: 'Ancash'},
            {value: 'Apurimac', displayValue: 'Apurimac'},
            {value: 'Arequipa', displayValue: 'Arequipa'},
            {value: 'Ayacucho', displayValue: 'Ayacucho'},
            {value: 'Cajamarca', displayValue: 'Cajamarca'},
            {value: 'Callao', displayValue: 'Callao'},
            {value: 'Cusco', displayValue: 'Cusco'},
            {value: 'Huancavelica', displayValue: 'Huancavelica'},
            {value: 'Huanuco', displayValue: 'Huanuco'}, 
            {value: 'Ica', displayValue: 'Ica'},
            {value: 'Junin', displayValue: 'Junin'},
            {value: 'La Libertad', displayValue: 'La Libertad'},
            {value: 'Lambayeque', displayValue: 'Lambayeque'},
            {value: 'Lima', displayValue: 'Lima'},
            {value: 'Loreto', displayValue: 'Loreto'},
            {value: 'Madre de Dios', displayValue: 'Madre de Dios'},
            {value: 'Moquegua', displayValue: 'Moquegua'},
            {value: 'Pasco', displayValue: 'Pasco'},
            {value: 'Piura', displayValue: 'Piura'},
            {value: 'Puno', displayValue: 'Puno'},
            {value: 'San Martin', displayValue: 'San Martin'},
            {value: 'Tacna', displayValue: 'Tacna'},
            {value: 'Tumbes', displayValue: 'Tumbes'},
            {value: 'Ucayali', displayValue: 'Ucayali'}
          ]
        },
        elementClasses: ['Half'],
        value: 'Lima',
        validation: {},
        valid: true,
        label: 'Departamento',
        errorMessage: ''
      },
      province: { 
        elementType: 'input', 
        elementConfig: {
          type: 'text',
          autoComplete: 'shipping locality'
        },
        elementClasses: ['Half'],
        value: '',
        validation: {
          required: true
        },
        valid: false,
        touched: false,
        label: 'Provincia',
        errorMessage: ''
      },
      district: { 
        elementType: 'input', 
        elementConfig: {
          type: 'text'
        },
        elementClasses: ['Half'],
        value: '',
        validation: {
          required: true
        },
        valid: false,
        touched: false,
        label: 'Distrito',
        errorMessage: ''
      },
      address: { 
        elementType: 'input', 
        elementConfig: {
          type: 'text',
          autoComplete: 'shipping street-address'
        },
        value: '',
        validation: {
          required: true
        },
        valid: false,
        touched: false,
        label: 'Dirección',
        errorMessage: ''
      },
      phone: { 
        elementType: 'input', 
        elementConfig: {
          type: 'tel',
          autoComplete: 'tel'
        },
        value: '',
        validation: {
          required: true
        },
        valid: false,
        touched: false,
        label: 'Teléfono',
        errorMessage: ''
      }
    },
    targetCurrency: {
      currency2: {
        elementType: 'select',
        elementConfig: {
          options: [
            {value: 'usd', displayValue: 'USD'},
            {value: 'pen', displayValue: 'PEN'},
            {value: 'btc', displayValue: 'BTC'}
          ]
        },
        value: 'usd',
        validation: {},
        valid: true
      }
    },
    sendingAccount: {},
    receivingAccount: {},
    step: 0,
    formIsValid: false,
    showModal: false,
    stepTitles: [
      '¿Qué operación deseas realizar?',
      'Cuéntanos un poco más sobre ti',
      '¿Qué moneda quieres?',
      '¿De qué banco envías?',
      '¿En qué banco quieres recibir?'
    ],
    style: {
      transform: 'translateX(0)', 
      opacity: 1
    }
  }

  componentWillMount() {
    const query = new URLSearchParams(this.props.location.search);

    const change = { ...this.state.change }

    for (let param of query.entries()) {
      change[param[0]].value = param[1]
      change[param[0]].valid = true
    }

    this.setState({change: change})

    const user = auth.currentUser

    const userRef = db.ref('users').child(user.uid)

    userRef.on('value', snapshot => {
      const userData = snapshot.val().personalData
      const userAccounts = snapshot.val().accounts

      let accounts = [];
      for (const key in userAccounts) {
        accounts = [
          ...accounts,
          {
            ...userAccounts[key],
            id: key
          }
        ]
      }

      const personalData = { ...this.state.personalData }

      if (userData.userType === 'natural') {
        delete personalData.ruc
        delete personalData.company
      }

      for (const key in personalData) {
        personalData[key].value = userData[key]
        personalData[key].valid = true
      }

      this.setState({personalData: personalData, accounts: accounts})
    })
  }

  submitFormHandler = event => {
    event.preventDefault()

    let formData = {}

    for (const key in this.state.change) {
      formData[key] = this.state.change[key].value;
    }

    for (const key in this.state.personalData) {
      formData[key] = this.state.personalData[key].value;
    }

    for (const key in this.state.targetCurrency) {
      formData[key] = this.state.targetCurrency[key].value;
    }

    formData = {
      ...formData,
      sendingAccount: this.state.sendingAccount,
      receivingAccount: this.state.receivingAccount
    }

    console.log(formData);
  }

  checkValidity (value, rules) {
    let isValid = true;

    if (rules.required) {
      isValid = value.trim() !== '' && isValid;
    }

    return isValid;
  }

  inputChangedHandler = (event, inputIdentifier, form) => {
    const updatedOrderForm = {
      ...this.state[form]
    }
    const updatedFormElement = {
      ...updatedOrderForm[inputIdentifier]
    }   

    updatedFormElement.value = event.target.value;
    updatedFormElement.valid = this.checkValidity(updatedFormElement.value, updatedFormElement.validation);
    updatedFormElement.touched = true;
    updatedOrderForm[inputIdentifier] = updatedFormElement;

    let formIsValid = true;
    for (let inputIdentifier in updatedOrderForm) {
      formIsValid = updatedOrderForm[inputIdentifier].valid && formIsValid;
    }

    this.setState({[form]: updatedOrderForm, formIsValid: formIsValid});
  }

  addStepHandler = () => {
    let formIsValid = true;
    for (let key in this.state[STEPS[this.state.step]]) {
      formIsValid = this.state[STEPS[this.state.step]][key].valid && formIsValid;
    }

    if (formIsValid) {
      let style = {
        transform: 'translateX(100vw)', 
        opacity: 0
      }
      this.setState({style: style})
  
      style = {
        transform: 'translateX(0)', 
        opacity: 1
      }
      setTimeout(() => this.setState({step: this.state.step + 1, style: style}), 300);
    }

    window.scroll({
      top: 0, 
      behavior: 'smooth' 
    })
  }

  subStepHandler = () => {
    let style = {
      transform: 'translateX(100vw)', 
      opacity: 0
    }
    this.setState({style: style})

    style = {
      transform: 'translateX(0)', 
      opacity: 1
    }
    setTimeout(() => this.setState({step: this.state.step - 1, style: style}), 300);

    window.scroll({
      top: 0, 
      behavior: 'smooth' 
    })
  }

  stepperHandler = () => {
    const formElementsArray = [];
    for (let key in this.state.change) {
      formElementsArray.push({
        id: key,
        config: this.state.change[key]
      })
    }

    const personalData = [];
    for (let key in this.state.personalData) {
      personalData.push({
        id: key,
        config: this.state.personalData[key]
      })
    }

    const targetCurrency = [];
    for (let key in this.state.targetCurrency) {
      targetCurrency.push({
        id: key,
        config: this.state.targetCurrency[key]
      })
    }

    let step = [];

    step[0] = formElementsArray.map(element => (
      <Input
        key={element.id}
        elementType={element.config.elementType}
        elementConfig={element.config.elementConfig}
        elementClasses={element.config.elementClasses}
        value={element.config.value}
        invalid={!element.config.valid}
        shouldValidate={element.config.validation}
        touched={element.config.touched}
        label={element.config.label}
        className={classes.FormWrapper}
        changed={(event) => this.inputChangedHandler(event, element.id, 'change')} />
    ))

    step[1] = personalData.map(element => (
      <Input
        key={element.id}
        elementType={element.config.elementType}
        elementConfig={element.config.elementConfig}
        elementClasses={element.config.elementClasses}
        value={element.config.value}
        invalid={!element.config.valid}
        shouldValidate={element.config.validation}
        touched={element.config.touched}
        label={element.config.label}
        changed={(event) => this.inputChangedHandler(event, element.id, 'personalData')} />
    ))

    step[2] = targetCurrency.map(element => (
      <Input
        key={element.id}
        elementType={element.config.elementType}
        elementConfig={element.config.elementConfig}
        elementClasses={element.config.elementClasses}
        value={element.config.value}
        invalid={!element.config.valid}
        shouldValidate={element.config.validation}
        touched={element.config.touched}
        label={element.config.label}
        changed={(event) => this.inputChangedHandler(event, element.id, 'targetCurrency')} />
    ))

    const sendingAccounts = this.state.accounts.map(account => {
      if (account.currency === this.state.change.currency1.value) {
        return (
          <SmallCard key={account.id} title={account.bank}>
            <Button btnType="Unformatted" linkTo="#" clicked={() => this.selectAccountHandler(account, 'sendingAccount')}>
              {account.name}<br/>
              {account.number}<br/>
              <span className={classes.Currency}>{account.currency}</span><br/>
              {account.owner}
            </Button>
          </SmallCard>
        )
      } else {
        return false
      }
    })

    step[3] = <div className={classes.AccountsWrapper}>{sendingAccounts} <SmallCard><Button btnType="Plain" linkTo="#" clicked={this.showModalHandler}>+ Agregar cuenta</Button></SmallCard></div>

    const receivingAccounts = this.state.accounts.map(account => {
      if (account.currency === this.state.targetCurrency.currency2.value) {
        return (
          <SmallCard key={account.id} title={account.bank}>
            <Button btnType="Unformatted" linkTo="#" clicked={() => this.selectAccountHandler(account, 'receivingAccount')}>
              {account.name}<br/>
              {account.number}<br/>
              <span className={classes.Currency}>{account.currency}</span><br/>
              {account.owner}
            </Button>
          </SmallCard>
        )
      } else {
        return false
      }
    })

    step[4] = <div className={classes.AccountsWrapper}>{receivingAccounts} <SmallCard><Button btnType="Plain" linkTo="#" clicked={this.showModalHandler}>+ Agregar cuenta</Button></SmallCard></div>

    return step
  }

  showModalHandler = () => {
    this.setState({showModal: !this.state.showModal})
  }

  selectAccountHandler = (account, type) => {
    this.setState({[type]: account})
  }

  render () {
    const stepper = this.stepperHandler();

    const modalContent = {
      title: 'Agregar cuenta',
      body: <Form type="AddAccount" submitButton="Agregar" />
    }

    const currentStep = this.state.step;

    const backButton = this.state.step > 0 ? <Button btnType="Info" clicked={this.subStepHandler} linkTo="#">Volver</Button> : null

    const nextButton = this.state.step === stepper.length-1 ? <Button btnType="Success" clicked={this.submitFormHandler}>Realizar pedido</Button> : <Button btnType="Success" clicked={this.addStepHandler} linkTo="#">Continuar</Button>;

    return (
      <Aux>
        <Modal title={modalContent.title} show={this.state.showModal} modalClosed={this.showModalHandler}>
          {modalContent.body}
        </Modal>
        <SavioStepper steps={['Cambio', 'Tú', 'Moneda', 'Envias de', 'Recibes en']} current={currentStep} />
        <div className={classes.SavioFlow}>
          <h2>{this.state.stepTitles[currentStep]}</h2>
          <div className={classes.Content}>
            <form onSubmit={this.submitFormHandler}>

              <div className={classes.FormWrapper} style={this.state.style}>
                {stepper[currentStep]}
              </div>
              
              {backButton}
              {nextButton}
              
            </form>
            
          </div>
        </div>
      </Aux>
    )
  }
}

export default SavioFlow;