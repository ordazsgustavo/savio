import React from 'react';
import { NavLink } from 'react-router-dom';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';

import classes from './NavigationItem.css';

const navigationItem = (props) => {
  let icon = null;
  if (props.icon) {
    icon = <FontAwesomeIcon icon={props.icon}/>
  }
  return(
    <li className={classes.NavigationItem} onClick={props.clicked}>
      <NavLink 
        to={props.link}
        exact={props.exact}
        activeClassName={classes.active}>{icon}<span className={classes.LinkText}>{props.children}</span></NavLink>
    </li>
  )
}
export default navigationItem;