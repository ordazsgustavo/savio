import React from 'react';

const footerColumn = (props) => (
  <div>
    <h3>{props.title}</h3>
    {props.children}
  </div>
)

export default footerColumn;