import React from 'react';

import classes from './MarketInfo.css';
import Charts from '../../../components/Charts/Charts';

const chart = () => {
  const data = {
    labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
    series: [
      [3.23, 3.225, 3.22, 3.215, 3.22, 3.235, 3.23, 3.225, 3.225, 3.23, 3.236, 3.237]
    ]
  }

  const options = {
    // Don't draw the line chart points
    showPoint: false,
    // Disable line smoothing
    lineSmooth: true,
    // X-Axis specific configuration
    axisX: {
      // We can disable the grid for this axis
      showGrid: false,
      // and also don't show the label
      showLabel: false
    },
    // Y-Axis specific configuration
    axisY: {
      // Lets offset the chart a bit from the labels
      // offset: 60
    },
    width: '100%',
    height: '100%'
  }

  const responsiveOptions = [
    ['screen and (min-width: 500px)', {
      axisX: {
        labelInterpolationFnc: function (value) {
          return value;
        }
      }
    }],
    ['screen and (max-width: 499px)', {
      axisX: {
        labelInterpolationFnc: function (value) {
          return value[0];
        }
      }
    }]
  ];
  return (
    <div className={classes.MarketInfo}>
      <div className={classes.Chart}>
        <Charts data={data} options={options} responsiveOptions={responsiveOptions} />
      </div>
      <div className={classes.Description}>
        <h3>Información del mercado</h3>
        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
      </div>
    </div>
  )
}

export default chart;