import React from 'react';
import { Link } from 'react-router-dom';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';

import classes from './NotificationItem.css';

const notificationItem = (props) => {
  let icon = null;
  if (props.icon) {
    icon = <FontAwesomeIcon icon={props.icon} />
  }

  const diffMs = (new Date() - new Date(props.since)); // milliseconds between now & generated
  const diffDays = Math.floor(diffMs / 86400000); // days
  const diffHrs = Math.floor((diffMs % 86400000) / 3600000); // hours
  const diffMins = Math.floor(((diffMs % 86400000) % 3600000) / 60000); // minutes
  const diffSec = Math.floor(((diffMs % 86400000) % 3600000) / 36000); // seconds
  let time = diffDays ? `${diffDays} d ` : '';
  time += diffHrs ? `${diffHrs} h ` : '';
  time += diffMins && !diffDays ? `${diffMins} m ` : '';
  time += diffSec && !diffHrs && !diffDays ? `${diffSec} s` : '';

  return (
    <div className={classes.NotificationItem}>
      <Link to={props.linkTo}>
        <div className={classes.Icon}>{icon}</div>
        <div className={classes.Content}>
          {props.content}
          <div className={classes.Time}>{time}</div>
        </div>
      </Link>
    </div>
  )
}

export default notificationItem;