import React, { Component } from 'react';
import { auth, db } from '../../../lib/firebase';

import classes from './Accounts.css';
import Aux from '../../../hoc/Aux/Aux';
import Header from '../../../components/UI/Header/Header';
import Card from '../../../components/UI/Card/Card';
import Input from '../../../components/UI/Input/Input';
import Button from '../../../components/UI/Button/Button';
import Modal from '../../../components/UI/Modal/Modal';
import Panel from '../../../components/UI/Panel/Panel';

class Accounts extends Component {
  state = {
    userId: '',
    accounts: [],
    showModal: false,
    loading: true,
    accountForm: {
      bank: {
        elementType: 'select',
        elementConfig: {
          options: [
            {value: 'BCP', displayValue: 'BCP'},
            {value: 'BBVA', displayValue: 'BBVA Continental'},
            {value: 'Financiero', displayValue: 'Banco Financiero'},
            {value: 'Interbank', displayValue: 'Interbank'},
            {value: 'Scotiabank', displayValue: 'Scotiabank'},
            {value: 'Citi Bank', displayValue: 'Citi Bank'},
            {value: 'Mi Banco', displayValue: 'Mi Banco'}
          ]
        },
        elementClasses: ['Third'],
        value: 'BCP',
        validation: {},
        valid: true
      },
      currency: {
        elementType: 'select',
        elementConfig: {
          options: [
            {value: 'pen', displayValue: 'PEN'},
            {value: 'usd', displayValue: 'USD'},
            {value: 'eur', displayValue: 'EUR'},
            {value: 'btc', displayValue: 'BTC'}
          ]
        },
        value: 'pen',
        validation: {},
        valid: true
      },
      number: {
        elementType: 'input', 
        elementConfig: {
          type: 'tel',
          placeholder: 'N. de cuenta'
        },
        value: '',
        validation: {
          required: true
        },
        valid: false,
        touched: false
      },
      owner: {
        elementType: 'input', 
        elementConfig: {
          type: 'text',
          placeholder: 'Titular'
        },
        value: '',
        validation: {
          required: true
        },
        valid: false,
        touched: false
      },
      city: {
        elementType: 'select',
        elementConfig: {
          options: [
            {value: 'arequipa', displayValue: 'Arequipa'},
            {value: 'ica', displayValue: 'Ica'},
            {value: 'lima', displayValue: 'Lima'},
            {value: 'la libertad', displayValue: 'La Libertad'},
            {value: 'lambayeque', displayValue: 'Lambayeque'},
            {value: 'piura', displayValue: 'Piura'}
          ]
        },
        value: 'arequipa',
        label: 'Ciudad de apertura',
        validation: {},
        valid: true
      }
    },
    formIsValid: false
  }

  componentWillMount() {
    console.log('[Accounts] Will Mount');
    let currentUserID = auth.currentUser.uid;
    this.setState({userId: currentUserID});

    let accountsRef = db.ref(`/agencys/${currentUserID}/accounts`);

    let accounts = []

    accountsRef.on('child_added', snap => {
      accounts = [
        ...accounts,
        {
          ...snap.val(),
          id: snap.key
        }
      ];

      this.setState({accounts: accounts});
    });
  }

  showModalHandler = () => {
    this.setState({showModal: !this.state.showModal});
  }

  checkValidity (value, rules) {
    let isValid = true;

    if (rules.required) {
      isValid = value.trim() !== '' && isValid;
    }

    if (rules.minLength) {
      isValid = value.length >= rules.minLength && isValid;
    }

    if (rules.maxLength) {
      isValid = value.length >= rules.minLength && isValid;
    }

    return isValid;
  }

  inputChangedHandler = (event, inputIdentifier) => {
    const updatedOrderForm = {
      ...this.state.accountForm
    }
    const updatedFormElement = {
      ...updatedOrderForm[inputIdentifier]
    }

    updatedFormElement.value = event.target.value;
    updatedFormElement.valid = this.checkValidity(updatedFormElement.value, updatedFormElement.validation);
    updatedFormElement.touched = true;
    updatedOrderForm[inputIdentifier] = updatedFormElement;

    let formIsValid = true;
    for (let inputIdentifier in updatedOrderForm) {
      formIsValid = updatedOrderForm[inputIdentifier].valid && formIsValid;
    }

    this.setState({accountForm: updatedOrderForm, formIsValid: formIsValid});
  }

  addAccountHandler = (event) => {
    event.preventDefault();
    this.setState({loading: true});
    const formData = {};
    const user = this.state.userId;

    for (const formElementIdentifier in this.state.accountForm) {
      formData[formElementIdentifier] = this.state.accountForm[formElementIdentifier].value;
    }

    const newAccount = db.ref(`/agencys/${user}/accounts`).push().key;

    let account = {
      ...formData
    }

    let updates = {};
    updates[`/agencys/${user}/accounts/${newAccount}`] = account;
    db.ref().update(updates);

    this.setState({showModal: !this.state.showModal});
  }

  render () {
    let panel = null;
    let cards = null;
    if (this.state.accounts.length === 0) {
      panel = (
        <Panel>No has agregado tus cuentas</Panel>
      );
    } else {
      console.log(this.state.accounts);
      cards = (
        this.state.accounts.map(account => (
          <Card 
            key={account.id} 
            header={account.bank}
            status={account.bank}>
            <p>Moneda: {account.currency}</p>
            <p>Numero de cuenta: {account.number}</p>
            <p>Titular: {account.owner}</p>
          </Card>
        ))
      );
    }

    const formElementsArray = [];
    for (let key in this.state.accountForm) {
      formElementsArray.push({
        id: key,
        config: this.state.accountForm[key]
      });
    }

    let form = null;

    form = (
      <form onSubmit={this.addAccountHandler}>      
        {formElementsArray.map(formElement => (
          <Input
            key={formElement.id}
            elementType={formElement.config.elementType}
            elementConfig={formElement.config.elementConfig}
            value={formElement.config.value}
            invalid={!formElement.config.valid}
            shouldValidate={formElement.config.validation}
            touched={formElement.config.touched}
            changed={(event) => this.inputChangedHandler(event, formElement.id)} />
        ))}
        <div className={classes.Buttons}>
          <Button 
            btnType="Success" 
            disabled={!this.state.formIsValid}
            clicked={this.addAccountHandler}>Agregar</Button>
        </div>
      </form>
    );

    return (
      <Aux>
        <Modal 
          centered
          show={this.state.showModal} 
          modalClosed={this.showModalHandler}>
          <p>Registra las cuentas donde los usuarios enviarán su importe</p>
          {form}
        </Modal>
        <Header>Mis cuentas</Header>
        <div className={classes.Accounts}>
          {panel}
          {cards}
          <Button
          btnType="Success"
          clicked={this.showModalHandler}>AGREGAR CUENTA</Button>
        </div>
      </Aux>
    )
  }
}

export default Accounts;