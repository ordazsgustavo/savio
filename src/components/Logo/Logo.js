import React from 'react';
import { Link } from 'react-router-dom';

import classes from './Logo.css';

const logo = (props) => {
  let styles = [classes.Logo]
  if (props.inverted) {
    styles.push(classes.Inverted)
  }
  return (
    <div className={styles.join(' ')}>
      <Link to="/">
        <h1>$AVIO</h1>
      </Link>
    </div>
  );
}

export default logo;