const ChangePassword = {
  actualPassword: { 
    elementType: 'input', 
    elementConfig: {
      type: 'password'
    },
    value: '',
    validation: {
      required: true
    },
    valid: true,
    touched: false,
    label: 'Contraseña actual',
    errorMessage: ''
  },
  newPassword: { 
    elementType: 'input', 
    elementConfig: {
      type: 'password',
      autoComplete: 'new-password'
    },
    value: '',
    validation: {
      required: true
    },
    valid: true,
    touched: false,
    label: 'Nueva contraseña',
    errorMessage: ''
  },
  repeatNewPassword: { 
    elementType: 'input', 
    elementConfig: {
      type: 'password'
    },
    value: '',
    validation: {
      required: true,
      equals: 'newPassword'
    },
    valid: true,
    touched: false,
    label: 'Repetir nueva contraseña',
    errorMessage: ''
  }
}

export default ChangePassword