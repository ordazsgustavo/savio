import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { auth, db, storage } from '../../../lib/firebase';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import placeholderImage from '../../../assets/images/placeholder.png';

import Peru from '../../../regulations/Peru';

import classes from './Orders.css';
import Aux from '../../../hoc/Aux/Aux';
import Header from '../../../components/UI/Header/Header';
import Spinner from '../../../components/UI/Spinner/Spinner';
import Panel from '../../../components/UI/Panel/Panel';
import Modal from '../../../components/UI/Modal/Modal';
import Input from '../../../components/UI/Input/Input';
import Card from '../../../components/UI/Card/Card';
import CardButton from '../../../components/UI/CardButton/CardButton';
import Button from '../../../components/UI/Button/Button';
import Tag from '../../../components/UI/Tag/Tag';

class Orders extends Component {
  state = {
    orders: [],
    orderDetails: {},
    loading: true,
    showModal: false,
    filter: 'on-hold',
    filterForm: {
      orderSatate: {
        elementType: 'radio',
        elementConfig: {
          type: 'radio',
          name: 'eta',
          radios: [
            {value: 'on-hold', label: 'En espera'},
            {value: 'processing', label: 'Procesando'},
            {value: 'completed', label: 'Completados'},
            {value: 'all', label: 'Todos'}
          ]
        },
        value: 'on-hold',
        validation: {},
        valid: true
      }
    },
    checkoutForm: {},
    images: [],
    formIsValid: false,
    loadingImage: false,
    timestamp: null
  }

  componentWillMount () {
    this.setState({checkoutForm: Peru})
    auth.onAuthStateChanged(user => {
      if (user) {
        const ordersRef = db.ref('/orders');
        
        let orders = [];

        ordersRef.orderByChild('user/id').equalTo(user.uid).on('value', snapshot => {
          orders = [];
          snapshot.forEach(snap => {
            if (snap.val().status.state === 'on-hold') {
              orders = [
                {
                  ...snap.val(),
                  id: snap.key
                },
                ...orders
              ]
            }
          });

          const time = Date.now();
          this.setState({loading: false, orders: orders, timestamp: time});
        });
      }
    })
  }

  resetCheckoutForm = () => {
    const checkoutForm = {
      ...this.state.checkoutForm
    }

    for (let key in this.state.checkoutForm) {
      checkoutForm[key].value = '';
      checkoutForm[key].valid = true;
      checkoutForm[key].touched = false;
    }

    this.setState({checkoutForm: checkoutForm});
  }

  orderDetailsHandler = id => {
    this.showModalHandler();
    this.state.orders.map(order => {
      if (order.id === id) {
        this.setState({orderDetails: order});
      }
      return true;
    })
  }

  filterOrdersHandler = (event, inputIdentifier) => {
    const updatedOrderForm = {
      ...this.state.filterForm
    }
    const updatedFormElement = {
      ...updatedOrderForm[inputIdentifier]
    }

    this.filterOrders(event.target.value);

    updatedFormElement.value = event.target.value;
    updatedOrderForm[inputIdentifier] = updatedFormElement;

    this.setState({filterForm: updatedOrderForm});
  }

  filterOrders = filter => {
    const userId = auth.currentUser.uid;
    if (userId) {
      const ordersRef = db.ref('/orders');

      let orders = [];

      this.setState({loading: true, filter: filter});

      if (filter === 'on-hold' || filter === 'processing' || filter === 'completed') {
        ordersRef.orderByChild('user/id').equalTo(userId).on('value', snapshot => {
          orders = [];
          snapshot.forEach(snap => {
            if (snap.val().status.state === filter) {
              orders = [
                {
                  ...snap.val(),
                  id: snap.key
                },
                ...orders
              ]
            }
          });
          this.setState({loading: false, orders: orders});            
        });
      }

      if (filter === 'all') {
        ordersRef.on('value', snapshot => {
          orders = [];
          snapshot.forEach(snap => {
            if (snap.val().user && snap.val().user.id === userId) {
              orders = [
                {
                  ...snap.val(),
                  id: snap.key
                },
                ...orders
              ]
            }
          })

          this.setState({loading: false, orders: orders});            
        });
      }
    }
  }

  showModalHandler = () => {
    if (this.state.showModal) {
      // ReactDOM.findDOMNode(this.refs.orderForm).reset()
      this.resetCheckoutForm();
    }
    this.setState({ showModal: !this.state.showModal, images: [], timestamp: Date.now() });
  }

  modalContentHandler = () => {
    let content = { 
      title: 'Ups!',
      body: 'Nada que ver aqui'
    }

    if (this.state.showModal) {
      const order = this.state.orderDetails;
      if (order.status.state === 'on-hold') {
        return content = {
          title: '¿Está seguro de que desea cancelar este pedido?',
          body: <Button btnType="Success" clicked={this.cancelOrderHandler}>CANCELAR PEDIDO</Button>
        }
      }

      if (order.status.state === 'processing') {
        if (order.status.steps.userTransferred) {
          return content = {
            title: 'Procesando',
            body: <p>Ya transferiste!</p>
          }
        } else {
          let formElementsArray = [];
          for (let key in this.state.checkoutForm) {
            formElementsArray = [...formElementsArray, {
              id: key,
              config: this.state.checkoutForm[key]
            }]
          }
          let images = (
            <Aux>
              {this.state.images[0] ? <img className={classes.Images} src={this.state.images[0]} alt=""/> : <img className={classes.Images} src={placeholderImage} alt=""/>}
              {this.state.images[1] ? <img className={classes.Images} src={this.state.images[1]} alt=""/> : <img className={classes.Images} src={placeholderImage} alt=""/>}
            </Aux>
          );
          if ((order.orderData.currency1 === 'usd' && order.orderData.ammount < 5000) || (order.orderData.currency2 === 'usd' && order.orderData.ammount2 < 5000)) {
            for (let index = 2; index < formElementsArray.length; index++) {
              delete formElementsArray[index];
            }
          }

          if (order.orderData.ammount < 200) {
            delete formElementsArray[0];
            images = (
              this.state.images[0] ? <img className={classes.Images} src={this.state.images[0]} alt=""/> : <img className={classes.Images} src={placeholderImage} alt=""/>
            )
          }

          if (this.state.checkoutForm.publicPerson.value === 'no') {
            delete formElementsArray[4];
            delete formElementsArray[5];
          }

          if (this.state.checkoutForm.operationTo.value === 'myself') {
            for (let i = 8; i < 17; i++) {
              delete formElementsArray[i];
            }
          } else if (this.state.checkoutForm.operationTo.value === 'naturalPerson') {
            for (let i = 11; i < 17; i++) {
              delete formElementsArray[i];
            }
          } else {
            delete formElementsArray[8];
            delete formElementsArray[9];
            delete formElementsArray[10];
          }

          const loadingImage = this.state.loadingImage ? <FontAwesomeIcon icon="circle-notch" spin /> : null;
          const body = (
            <Aux>
              <p>Debes transferir: <span>{order.orderData.ammount2}</span> <span className={classes.Currency}>{order.orderData.currency2}</span> a la cuenta: </p>
              <p>N#:</p>
              <p>A nombre de:</p>
              <br/>
              <p>Completa los siguentes campos para realizar tu pedido.</p>

              <div className={classes.ImagesWrapper}>
                {images}
                {loadingImage}
              </div>
              
              <form 
                key={this.state.timestamp}
                ref="orderForm"
                onSubmit={this.acceptOrderHandler}>      
                {formElementsArray.map(formElement => (
                  <Input
                    key={formElement.id}
                    elementType={formElement.config.elementType}
                    elementConfig={formElement.config.elementConfig}
                    value={formElement.config.value}
                    invalid={!formElement.config.valid}
                    shouldValidate={formElement.config.validation}
                    touched={formElement.config.touched}
                    label={formElement.config.label}
                    changed={(event) => this.inputChangedHandler(event, formElement.id)} />
                ))}
                <p>Al hacer click en Aceptar, estás aceptando los Términos y Condiciones.</p>
                <Button 
                  btnType="Success" 
                  disabled={!this.state.formIsValid}
                  clicked={this.acceptOrderHandler}>ACEPTAR</Button>
              </form>
            </Aux>
          )

          return content = { 
            title:  <h3>¡La agencia {order.agency.displayName} ha tomado tu pedido!</h3>,
            body: body
          }
        }
      }

      if (order.status.state === 'completed') {
        return content = {
          title: '¡Completado!',
          body: <p>Tu pedido ha sido completado, revisa tus estados de cuenta, si tienes alguna consulta escribenos a info@sav.io, o a traves de nustro chat virtual.</p>
        }
      }
    }

    return content;
  }

  checkValidity (value, rules) {
    let isValid = true;

    if (rules.required) {
      isValid = value.trim() !== '' && isValid;
    }

    if (rules.minLength) {
      isValid = value.length >= rules.minLength && isValid;
    }

    if (rules.maxLength) {
      isValid = value.length >= rules.minLength && isValid;
    }

    return isValid;
  }

  inputChangedHandler = (event, inputIdentifier) => {
    let updatedOrderForm = {
      ...this.state.checkoutForm
    }
    const updatedFormElement = {
      ...updatedOrderForm[inputIdentifier]
    }
    
    updatedFormElement.value = event.target.value;
    updatedFormElement.valid = this.checkValidity(updatedFormElement.value, updatedFormElement.validation);
    updatedFormElement.touched = true;
    updatedOrderForm[inputIdentifier] = updatedFormElement;

    let formIsValid = true;
    for (let inputIdentifier in updatedOrderForm) {
      formIsValid = updatedOrderForm[inputIdentifier].valid && formIsValid;
    }

    this.setState({checkoutForm: updatedOrderForm, formIsValid: formIsValid});

    if (inputIdentifier === 'document' || inputIdentifier === 'voucher') {
      // Get file
      const file = event.target.files[0];

      // Create storage ref
      const storageRef = storage.ref('orders');
      const childRef = storageRef.child(`${this.state.orderId}/${inputIdentifier}-${file.name}`);

      let task = childRef.put(file);

      task.on('state_changed', 
        snapshot => { // In progress
          // const percentage = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
          this.setState({loadingImage: true});
        },
        error => { // Error
          console.log(error);
        },
        () => { // Completed
          let downloadURL = task.snapshot.downloadURL;

          let images = [...this.state.images, downloadURL];

          this.setState({images: images, loadingImage: false});
        }
      );
    }
  }

  acceptOrderHandler = event => {
    event.preventDefault();
    this.setState({loading: true});
    const formData = {};
    const user = auth.currentUser;

    for (const formElementIdentifier in this.state.checkoutForm) {
      if (this.state.checkoutForm[formElementIdentifier].value !== '') {
        formData[formElementIdentifier] = this.state.checkoutForm[formElementIdentifier].value;
      }
    }

    if (user) {
      const rootRef = db.ref('orders').child(this.state.orderDetails.id);

      const generated = new Date();
      const agencyNotification = {
        generated: generated,
        content: `El usuario ha aceptado el pedido`,
        linkTo: `/order/details?id=${this.state.orderDetails.id}`
      }

      let updates = {};
      updates[`/orderData/files`] = this.state.images;
      updates[`/sbsForm`] = formData;
      updates[`/status/steps/userTransferred`] = generated;
      updates[`/notifications/agencys/${this.state.orderDetails.agency.id}`] = agencyNotification;
      
      rootRef.update(updates);
      ReactDOM.findDOMNode(this.refs.orderForm).reset();
    } else {
      console.log('Not signed in'); 
    }
    this.showModalHandler();
  }

  cancelOrderHandler = () => {
    const user = auth.currentUser;

    if (user) {
      // User is signed in.);
      const rootRef = db.ref(`orders/${this.state.orderDetails.id}`);

      let updates = {};
      updates[`/status/state`] = 'cancelled';

      rootRef.update(updates);
    } else {
      // No user is signed in.
      console.log('Not signed in'); 
    }

    this.setState({showModal: false});  
  }

  listOrders = () => {
    let content = '';
    let button = null;
    return (
      this.state.orders.map(order => {
        let header = null;
        if (order.status.state === 'on-hold') {

          if (order.orderData.operation === 'want') {
            content = <p>Quiero {order.orderData.ammount} <span className={classes.Currency}>{order.orderData.currency1}</span>, debo enviar <span className={classes.Currency}>{order.orderData.currency2}</span></p> 
          } else {
            content = <p>Tengo {order.orderData.ammount} <span className={classes.Currency}>{order.orderData.currency1}</span>, espero recibir <span className={classes.Currency}>{order.orderData.currency2}</span></p>
          }

          button = (
            <Aux>
             <CardButton clicked={() => this.orderDetailsHandler(order.id)} multi>CANCELAR</CardButton>
             <CardButton linkTo={`/orders/offers?id=${order.id}`} multi>Ver Ofertas</CardButton>
            </Aux>
          )

          // const offers = Object.keys(order.offers).length;
          let count = 0;
          for (const key in order) {
            if (order.hasOwnProperty(key) && key === 'offers') {
              count = Object.keys(order.offers).length
            }
          }

          header = (
            <Aux>
              {order.id}
              <Tag type="Warning">{count} {count === 1 ? 'oferta' : 'ofertas'}</Tag>
            </Aux>
          )

        } else if (order.status.state === 'processing') {

          if (order.orderData.operation === 'want') { 
              content = <p>Quiero {order.orderData.ammount} <span className={classes.Currency}>{order.orderData.currency1}</span> <br/> La agencia <strong>{order.agency.displayName}</strong> te solicita {order.orderData.ammount2} <span className={classes.Currency}>{order.orderData.currency2}</span></p>
          } else {
              content = <p>Tengo {order.orderData.ammount} <span className={classes.Currency}>{order.orderData.currency1}</span> <br/> La agencia <strong>{order.agency.displayName}</strong> te ofrece {order.orderData.ammount2} <span className={classes.Currency}>{order.orderData.currency2}</span></p>
          }

          button = (
            <Aux>
              <CardButton linkTo={`${this.props.match.path}/order-status/?id=${order.id}`} multi>ESTADO</CardButton>
              <CardButton clicked={() => this.orderDetailsHandler(order.id)} multi>DETALLES</CardButton>
            </Aux>
          );

          header = order.id

        } else if (order.status.state === 'completed') {

          if (order.orderData.operation === 'want') {
            content = <p>Quiería {order.orderData.ammount} <span className={classes.Currency}>{order.orderData.currency1}</span>, envié {order.orderData.ammount2} <span className={classes.Currency}>{order.orderData.currency2}</span><br/>
            Con la agencia: {order.agency.displayName}</p>
          } else {
            content = <p>Tenía {order.orderData.ammount} <span className={classes.Currency}>{order.orderData.currency1}</span>, recibí {order.orderData.ammount2} <span className={classes.Currency}>{order.orderData.currency2}</span><br/>
            Con la agencia: {order.agency.displayName}</p> 
          }

          button = <CardButton linkTo={`${this.props.match.path}/order-status/?id=${order.id}`}>DETALLES</CardButton> 
          
          header = order.id

        } else if (order.status.state === 'cancelled') {

          if (order.orderData.operation === 'want') {
            content = <p>Quiería {order.orderData.ammount} <span className={classes.Currency}>{order.orderData.currency1}</span>, debía enviar {order.orderData.ammount2} <span className={classes.Currency}>{order.orderData.currency2}</span></p>
          } else {
            content = <p>Tenía {order.orderData.ammount} <span className={classes.Currency}>{order.orderData.currency1}</span> <br/> La agencia <strong>{order.agency.displayName}</strong> me ofrecía {order.orderData.ammount2} <span className={classes.Currency}>{order.orderData.currency2}</span></p>
          }

          button = null;

          header = order.id

        }

        return (
          <Card 
            key={order.id}
            header={header}
            status={order.status.state}
            button={button}>
            {content}
            {order.status.state === 'processing' ? <p className={classes.SmallMessage}>Click en detalles para ver oferta de la agencia.</p> : null}
          </Card>
        )
      })
    )
  }

  render () {
    let orders = null;
    let panel = null;    
    if (this.state.loading) {
      orders = <Spinner />
    } else {
      if (this.state.orders.length === 0) {
        const filter = this.state.filter;

        if (filter === 'on-hold') {
          panel = <Panel>No hay pedidos en espera</Panel>
        }
        if (filter === 'processing') {
          panel = <Panel>No hay pedidos en proceso</Panel>
        }
        if (filter === 'completed') {
          panel = <Panel>No hay pedidos completados</Panel>
        }
        if (filter === 'all') {
          panel = <Panel>No hay pedidos</Panel>
        }
      } else {
        orders = this.listOrders();
      }
    }

    const filterForm = this.state.filterForm;

    const modalContent = this.modalContentHandler();

    return (
      <Aux>
        <Modal title={modalContent.title} show={this.state.showModal} modalClosed={this.showModalHandler}>
          {modalContent.body}
        </Modal>
        <Header>Pedidos</Header>
        <div className={classes.Orders}>
          <Input
              elementType={filterForm.orderSatate.elementType}
              elementConfig={filterForm.orderSatate.elementConfig}
              value={filterForm.orderSatate.value}
              invalid={!filterForm.orderSatate.valid}
              shouldValidate={filterForm.orderSatate.validation}
              touched={filterForm.orderSatate.touched}
              changed={(event) => this.filterOrdersHandler(event, 'orderSatate')} /> 
          {panel}
          {orders}
        </div>
      </Aux>
    )
  }
}

export default Orders;