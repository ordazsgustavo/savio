import React from 'react';

import classes from './Header.css';

const header = (props) => (
  <div className={classes.Header}><p>{props.children}</p></div>
)

export default header;