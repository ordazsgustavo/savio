import React from 'react';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';

import classes from './Spinner.css';

const spinner = () => (
  <div className={classes.Spinner}>
    <FontAwesomeIcon icon='circle-notch' spin />
  </div>
)

export default spinner;