import React, { Component } from 'react';
import { auth, db } from '../../../lib/firebase';

import classes from './Orders.css';
import Aux from '../../../hoc/Aux/Aux';
import Card from '../../../components/UI/Card/Card';
import Modal from '../../../components/UI/Modal/Modal';
import Header from '../../../components/UI/Header/Header';
import Spinner from '../../../components/UI/Spinner/Spinner';
import Input from '../../../components/UI/Input/Input';
import Button from '../../../components/UI/Button/Button';
import CardButton from '../../../components/UI/CardButton/CardButton';
import Panel from '../../../components/UI/Panel/Panel';

class Orders extends Component {
  state = {
    orders: [],
    orderDetails: {},
    rates: {},
    account: {},
    loading: true,
    showModal: false,
    images: [],
    filter: 'on-hold',
    filterForm: {
      orderSatate: {
        elementType: 'radio',
        elementConfig: {
          type: 'radio',
          name: 'eta',
          radios: [
            {value: 'on-hold', label: 'En espera'},
            {value: 'processing', label: 'Procesando'},
            {value: 'completed', label: 'Completados'},
            {value: 'all', label: 'Todos'}
          ]
        },
        value: 'on-hold',
        validation: {},
        valid: true
      }
    },
    takeOrderForm: {
      rateType: {
        elementType: 'radio',
        elementConfig: {
          type: 'radio',
          name: 'rateType',
          radios: [
            { value: 'day', label: 'Dia' },
            { value: 'pref', label: 'Preferencial' }
          ]
        },
        value: 'day',
        validation: {},
        valid: true
      },
      pref: {
        elementType: 'input',
        elementConfig: {
          type: 'text'
        },
        value: '',
        validation: {
          required: true,
          disabled: true
        },
        valid: false,
        touched: false,
        label: 'Preferencial'
      },
      agencyAccount: { 
        elementType: 'select',
        elementConfig: {
          options: [
            {value: '', displayValue: 'Selecciona'}
          ]
        },
        value: '',
        validation: {},
        valid: true
      },
      eta: {
        elementType: 'radio',
        elementConfig: {
          type: 'radio',
          name: 'eta',
          radios: [
            { value: '0-10', label: '0-10' },
            { value: '11-15', label: '11-15' },
            { value: '16-20', label: '16-20' },
            { value: '21-30', label: '21-30' },
            { value: '31-40', label: '31-40' }
          ]
        },
        value: '11-15',
        validation: {},
        valid: true
      }
    },
    formIsValid: false,
    error: null
  }

  // shouldComponentUpdate(nextProps, nextState) {
  //   return nextState.orders !== this.state.orders || nextState.showModal !== this.state.showModal || nextState.takeOrderForm !== this.state.takeOrderForm || nextState.filterForm !== this.state.filterForm || nextState.images !== this.state.images;
  // }

  componentWillMount() {
    auth.onAuthStateChanged(user => {
      if (user) {
        const ordersRef = db.ref('/orders');
        const agencyRef = db.ref('/agencys').child(user.uid);

        let orders = [];

        agencyRef.on('value', agencySnap => {
          this.setState({rates: agencySnap.val().rates})

          const accounts = agencySnap.val().accounts;

          const takeOrderForm = {...this.state.takeOrderForm};
          const agencyAccount = {...takeOrderForm.agencyAccount}

          for (const key in accounts) {
            if (accounts.hasOwnProperty(key)) {
              agencyAccount.elementConfig.options = [
                ...agencyAccount.elementConfig.options,
                {value: key, displayValue: accounts[key].bank}
              ]
            }
          }

          takeOrderForm.agencyAccount = agencyAccount;

          ordersRef.orderByChild('status/state').equalTo('on-hold').on('value', snapshot => {
            orders = [];
            snapshot.forEach(snap => {
              orders = [
                {
                  ...snap.val(),
                  id: snap.key
                },
                ...orders
              ]           
            })
            this.setState({ loading: false, orders: orders, takeOrderForm: takeOrderForm });
          })
        })
      }
    })
  }

  showModalHandler = () => {
    this.setState({ showModal: !this.state.showModal });
  }

  orderDetailsHandler = id => {
    this.showModalHandler();

    console.log(id);

    this.state.orders.map(order => {
      if (order.id === id) {
        this.setState({orderDetails: order})
        db.ref(`users/${order.user.id}/accounts/${order.orderData.receivingBank}`).on('value', snap => {
          this.setState({account: snap.val()})
          console.log(snap.val());
        })
      }
      return true
    })
  }

  modalContentHandler = () => {
    if (this.state.showModal) {
      const order = this.state.orderDetails;

      if (order.status.state === 'on-hold') {
        const user = auth.currentUser;

        for (const key in order.offers) {
          if (order.offers.hasOwnProperty(key) && key === user.uid) {
            console.log('[hasOwnProperty]');
            return {
              title: 'Nuevo pedido',
              body: <p>Ya has hecho una oferta</p>
            }
          }
        }

        const formElementsArray = [];
        for (let key in this.state.takeOrderForm) {
          formElementsArray.push({
            id: key,
            config: this.state.takeOrderForm[key]
          });
        }
  
        let inputs = formElementsArray.map(formElement => (
          <Input
            elementType={formElement.config.elementType}
            elementConfig={formElement.config.elementConfig}
            elementClasses={formElement.config.elementClasses}
            value={formElement.config.value}
            invalid={!formElement.config.valid}
            shouldValidate={formElement.config.validation}
            touched={formElement.config.touched}
            label={formElement.config.label}
            changed={(event) => this.inputChangedHandler(event, formElement.id)} />
        ));

        let currencySendsUser = null;
        let currencyReceivesUser = null;
        let ammountSendsUser = null;
        let ammountReceivesUser = null;
        if (order.orderData.operation === 'want') {
          currencySendsUser = order.orderData.currency2;
          currencyReceivesUser = order.orderData.currency1;
          ammountSendsUser = this.calcMontoRecibe();
          ammountReceivesUser = order.orderData.ammount;
        } else {
          currencySendsUser = order.orderData.currency1;
          currencyReceivesUser = order.orderData.currency2;
          ammountSendsUser = order.orderData.ammount;
          ammountReceivesUser = this.calcMontoRecibe();
        }
        const comission = ((0.15 * ammountSendsUser) / 100).toFixed(2);

        const rate = this.rate();
        let day = 0;
        for (const key in this.state.rates) {
          if (this.state.rates.hasOwnProperty(key) && key === rate) {
            day = this.state.rates[key];
          }
        }

        const body = (
          <Aux>
            <h3>Cliente</h3>
            <p>
              <strong>{order.user.docType}</strong>: {order.user.document}<br/>
              <strong>Nombre</strong>: {order.user.name}<br/>
              <strong>Apellido</strong>: {order.user.lastName}<br/>
              <strong>Envía de</strong>: {order.orderData.sendingBank}<br/>
              <strong>Recibe en</strong>: {this.state.account ? this.state.account.bank : order.orderData.receivingBank}
            </p>
            <h3>Pedido</h3>
            <p>
              <strong>Operación:</strong> {order.orderData.operationType} <br/>
              <strong>Recibes:</strong> {ammountSendsUser} <span className={classes.Currency}>{currencySendsUser}</span> <br/><br/>
              <strong>Tasa</strong>:
            </p>
            <br/>
            <form onSubmit={this.takeOrderHandler}>
              {inputs[0]}
              <p>
                <strong>Día:</strong> {day}<br/>
                <strong>Preferencial:</strong>
              </p>
              {inputs[1]}
              <p>
                <strong>Envías:</strong> {ammountReceivesUser} <span className={classes.Currency}>{currencyReceivesUser}</span>
              </p>
              <br/>
              <p>
                <strong>Comisión SAVIO</strong>: {comission} <span className={classes.Currency}>{currencySendsUser}</span><br/>
              </p>
              <p><strong>Recibo en</strong>:</p>
              {inputs[2]}
              <p>
                <strong>Tiempo estimado (en minutos)</strong>:
              </p>
              {inputs[3]}
              <Button btnType="Success" clicked={this.takeOrderHandler}>Enviar</Button>
            </form>
          </Aux>
        )

        return  {
          title: 'Nuevo pedido',
          body: body
        }
      }

      if (order.status.state === 'processing') {

        if (!order.status.steps.userTransferred) {
          return {
            title: 'Procesando',
            body: <p>Esperando transferencia del usuario</p>
          }
        } else {
          const styles = {
            height: '100px',
            margin: '10px 15px'
          }
          
          const body = (
            <Aux>
              <p>
                <strong>El usuario ha abonado:</strong> {order.orderData.operation === 'want' ? <span>{order.orderData.ammount2} <span className={classes.Currency}>{order.orderData.currency2}</span></span> : <span>{order.orderData.ammount} <span className={classes.Currency}>{order.orderData.currency1}</span></span>}, desde {order.orderData.sendingBank}
              </p>
              <p><strong>Comprobantes:</strong></p>
              {order.orderData.files[0] ? <img style={styles} src={order.orderData.files[0]} alt="document"/> : null}
              {order.orderData.files[1] ? <img style={styles} src={order.orderData.files[1]} alt="voucher"/> : null}
              <p><strong>Debes enviar:</strong> {order.orderData.operation === 'want' ? <span>{order.orderData.ammount} <span className={classes.Currency}>{order.orderData.currency1}</span></span> : <span>{order.orderData.ammount2} <span className={classes.Currency}>{order.orderData.currency2}</span></span>}</p>
              <p>
                <strong>A la cuenta:</strong> <br/>
                <strong>Banco:</strong> {this.state.account ? this.state.account.bank : order.orderData.receivingBank}  <br/>
                <strong>N de cuenta:</strong> {order.orderData.accountNumber} <br/>
                <strong>A nombre de:</strong> {this.state.account ? this.state.account.name : 'Not given :('}
              </p>
              <Button btnType="Success" clicked={this.completedOrderHandler}>COMPLETADO</Button>
            </Aux>
          )

          return {
            title: 'Procesando',
            body: body
          }
        }
      }

      if (order.status.state === 'completed') {
        const diffMs = (new Date(order.status.steps.generated) - new Date(order.status.steps.agencyTransferred)), // milliseconds between now & generated
        diffDays = Math.floor(diffMs / 86400000), // days
        diffHrs = Math.floor((diffMs % 86400000) / 3600000), // hours
        diffMins = Math.floor(((diffMs % 86400000) % 3600000) / 60000), // minutes
        diffSec = Math.floor(((diffMs % 86400000) % 3600000) / 36000); // seconds
        let time = diffDays ? `${diffDays} d ` : '';
        time += diffHrs ? `${diffHrs} h ` : '';
        time += diffMins && !diffDays ? `${diffMins} m ` : '';
        time += diffSec && !diffHrs && !diffDays ? `${diffSec} s` : '';

        const body = (
          <Aux>
            <p>El pedido ha sido completado con éxito!</p>
            <p>Recibiste: {order.orderData.ammount} {order.orderData.ammount}</p>
            <p>Enviaste: {order.orderData.montoRecibe} {order.orderData.monedaRecibe}</p>
            <p>Tiempo: {time}</p>
          </Aux>
        )

        return {
          title: '¡Completado!',
          body: body
        }
      }
    }

    return {
      title: 'Ups!',
      body: 'Nada que ver aqui'
    }
  }

  takeOrderHandler = event => {
    event.preventDefault();
    const user = auth.currentUser;
    const formData = {};
    const order = this.state.orderDetails;

    const rate = this.rate();

    for (const formElementIdentifier in this.state.takeOrderForm) {
      formData[formElementIdentifier] = this.state.takeOrderForm[formElementIdentifier].value;
    }

    const rootRef = db.ref('orders').child(order.id);

    if (user) {
      let day = 0;
      for (const key in this.state.rates) {
        if (this.state.rates.hasOwnProperty(key) && key === rate) {
          day = this.state.rates[key];
        }
      }
      const offer = {
        eta: formData.eta,
        rate: {
          day: day,
          pref: formData.pref !== '' ? formData.pref : 'no-pref'
        },
        comission: ((0.15 * order.orderData.ammount) / 100).toFixed(2),
        displayName: user.displayName,
        account: formData.agencyAccount
      }

      const generated = new Date();

      const agencyMadeOffer = {
        generated: generated,
        displayName: user.displayName
      }

      const userNotification = {
        generated: generated,
        content: `La agencia ${user.displayName} ha hecho una oferta`,
        linkTo: `/order/details?id=${this.state.orderDetails.id}`
      }

      let updates = {};
      updates[`/offers/${user.uid}`] = offer;
      updates[`/status/steps/agencyMadeOffer/${user.uid}`] = agencyMadeOffer;
      updates['/orderData/ammount2'] = this.calcMontoRecibe();
      updates[`/notifications/users/${this.state.orderDetails.user.id}`] = userNotification;

      rootRef.update(updates);
    }

    this.setState({ showModal: false });
  }

  completedOrderHandler = () => {
    const user = auth.currentUser;
    if (user) {
      const rootRef = db.ref(`orders/${this.state.orderDetails.id}`);
      
      const generated = new Date();
      const userNotification = {
        generated: generated,
        content: `La agencia ${user.displayName} ha completado el pedido`,
        linkTo: `/order/details?id=${this.state.orderDetails.id}`
      }

      let updates = {};
      updates['/status/state'] = 'completed';
      updates['/status/steps/agencyTransferred'] = generated;
      updates[`/notifications/users/${this.state.orderDetails.user.id}`] = userNotification;

      rootRef.update(updates);
    }

    this.setState({ showModal: false });
  }

  checkValidity(value, rules) {
    let isValid = true;

    if (rules.required) {
      isValid = value.trim() !== '' && isValid;
    }

    return isValid;
  }

  inputChangedHandler = (event, inputIdentifier) => {
    const updatedOrderForm = {
      ...this.state.takeOrderForm
    }
    const updatedFormElement = {
      ...updatedOrderForm[inputIdentifier]
    }

    if (inputIdentifier === 'rateType') {
      const updatedRateType = {
        ...updatedOrderForm['pref']
      }

      if (event.target.value === 'day') {
        updatedRateType.elementConfig.disabled = true;
        updatedRateType.value = '';
      } else {
        updatedRateType.elementConfig.disabled = false;
      }

      updatedOrderForm['pref'] = updatedRateType;
    }

    updatedFormElement.value = event.target.value;
    updatedFormElement.valid = this.checkValidity(updatedFormElement.value, updatedFormElement.validation);
    updatedFormElement.touched = true;
    updatedOrderForm[inputIdentifier] = updatedFormElement;

    let formIsValid = true;
    for (let inputIdentifier in updatedOrderForm) {
      formIsValid = updatedOrderForm[inputIdentifier].valid && formIsValid;
    }

    this.setState({ takeOrderForm: updatedOrderForm, formIsValid: formIsValid });
  }

  filterOrdersHandler = (event, inputIdentifier) => {
    const updatedOrderForm = {
      ...this.state.filterForm
    }
    const updatedFormElement = {
      ...updatedOrderForm[inputIdentifier]
    }

    this.filterOrders(event.target.value);

    updatedFormElement.value = event.target.value;
    updatedOrderForm[inputIdentifier] = updatedFormElement;

    this.setState({ filterForm: updatedOrderForm });
  }

  filterOrders = filter => {
    const user = auth.currentUser;
    if (user.uid) {
      const ordersRef = db.ref('/orders');

      let orders = [];

      this.setState({ loading: true, filter: filter });

      if (filter === 'on-hold') {
        ordersRef.orderByChild('status/state').equalTo('on-hold').on('value', snapshot => {
          orders = [];
          snapshot.forEach(snap => {
            orders = [
              {
                ...snap.val(),
                id: snap.key
              },
              ...orders
            ]
          });

          this.setState({ loading: false, orders: orders });
        });
      }

      if (filter === 'processing' || filter === 'completed') {
        ordersRef.orderByChild('agency/id').equalTo(user.uid).on('value', snapshot => {
          orders = [];
          snapshot.forEach(snap => {
            if (snap.val().status.state === filter) {
              orders = [
                {
                  ...snap.val(),
                  id: snap.key
                },
                ...orders
              ]
            }
          });
          this.setState({ loading: false, orders: orders });
        });
      }

      if (filter === 'all') {
        ordersRef.on('value', snapshot => {
          orders = [];
          snapshot.forEach(snap => {
            if (snap.val().status.state === 'on-hold') {
              orders = [
                {
                  ...snap.val(),
                  id: snap.key
                },
                ...orders
              ]
            }
            if (snap.val().agency && snap.val().agency.id === user.uid) {
              orders = [
                {
                  ...snap.val(),
                  id: snap.key
                },
                ...orders
              ]
            }
          })

          this.setState({ loading: false, orders: orders });
        });
      }
    }
  }

  calcExchange = (operation, type, rate, ammount) => {
    let total = 0;
    if (operation === 'want') {
      if (type === 'compra') {
        total = (ammount / rate).toFixed(2);
      }
  
      if (type === 'venta') {
        total = (ammount * rate).toFixed(2);
      }
    } else {
      if (type === 'compra') {
        total = (ammount * rate).toFixed(2);
      }
  
      if (type === 'venta') {
        total = (ammount / rate).toFixed(2);
      }
    }
    return total;
  }

  calcMontoRecibe = () => {
    const order = this.state.orderDetails;
    const takeOrderForm = this.state.takeOrderForm;
    let montoRecibe = 0;

    if (takeOrderForm.rateType.value === 'day') {
      const rate = this.rate();
      for (const key in this.state.rates) {
        if (this.state.rates.hasOwnProperty(key) && key === rate) {
          return this.calcExchange(order.orderData.operation, order.orderData.operationType, this.state.rates[rate], order.orderData.ammount);
        }
      }
      
      // this.setState({error: 'No hay tipo de cambio registrado'})
    } else if (takeOrderForm.rateType.value === 'pref') {
      return montoRecibe = this.calcExchange(order.orderData.operation, order.orderData.operationType, takeOrderForm.pref.value, order.orderData.ammount);
    }

    return montoRecibe;
  }

  rate = () => {
    const order = this.state.orderDetails;
    let currencySendsUser = null;
    let currencyReceivesUser = null;
    if (order.orderData.operation === 'want') {
      currencySendsUser = order.orderData.currency2;
      currencyReceivesUser = order.orderData.currency1;
    } else {
      currencySendsUser = order.orderData.currency1;
      currencyReceivesUser = order.orderData.currency2;
    }

    return `${currencySendsUser}2${currencyReceivesUser}`;
  }

  listOrders = () => {
    return this.state.orders.map(order => {
      let content = '';
      const state = order.status.state;

      if (state === 'on-hold') {
        if (order.orderData.operation === 'want') {
          content = (
            <p>
              <strong>Operación</strong>: {order.orderData.operationType} <br />
              <strong>Recibes:</strong> <span className={classes.Currency}>{order.orderData.currency2}</span> <br/>
              <strong>Envías:</strong> {order.orderData.ammount} <span className={classes.Currency}>{order.orderData.currency1}</span>
            </p>
          )
        } else {
          content = (
            <p>
              <strong>Operación</strong>: {order.orderData.operationType} <br />
              <strong>Recibes:</strong> {order.orderData.ammount} <span className={classes.Currency}>{order.orderData.currency1}</span> <br/>
              <strong>Envías:</strong> <span className={classes.Currency}>{order.orderData.currency2}</span>
            </p>
          )
        }
      }

      if (state === 'processing') {
        if (order.orderData.operation === 'want') {
          content = (
            <p>
              <strong>Operación:</strong> {order.orderData.operationType} <br />
              <strong>Recibes:</strong> {order.orderData.ammount2} <span className={classes.Currency}>{order.orderData.currency2}</span> <br/>
              <strong>Envías:</strong> {order.orderData.ammount} <span className={classes.Currency}>{order.orderData.currency1}</span>
            </p>
          )
        } else {
          content = (
            <p>
              <strong>Operación:</strong> {order.orderData.operationType} <br />
              <strong>Recibes:</strong> {order.orderData.ammount} <span className={classes.Currency}>{order.orderData.currency1}</span> <br/>
              <strong>Envías:</strong> {order.orderData.ammount2} <span className={classes.Currency}>{order.orderData.currency2}</span>
            </p>
          )
        }
      }

      if (state === 'completed') {
        if (order.orderData.operation === 'want') {
          content = (
            <p>
              <strong>Operación:</strong> {order.orderData.operationType} <br />
              <strong>Recibiste:</strong> {order.orderData.ammount2} <span className={classes.Currency}>{order.orderData.currency2}</span> <br/>
              <strong>Enviaste:</strong> {order.orderData.ammount} <span className={classes.Currency}>{order.orderData.currency1}</span>
            </p>
          )
        } else {
          content = (
            <p>
              <strong>Operación:</strong> {order.orderData.operationType} <br />
              <strong>Recibiste:</strong> {order.orderData.ammount} <span className={classes.Currency}>{order.orderData.currency1}</span> <br/>
              <strong>Enviaste:</strong> {order.orderData.ammount2} <span className={classes.Currency}>{order.orderData.currency2}</span>
            </p>
          )
        }
      }

      if (state === 'cancelled') {
        if (order.orderData.operation === 'want') {
          content = (
            <p>
              <strong>Operación</strong>: {order.orderData.operationType} <br />
              <strong>Recibes:</strong> {order.orderData.currency2} <br/>
              <strong>Envías</strong> {order.orderData.ammount} {order.orderData.currency1}
            </p>
          );
        } else {
          content = (
            <p>
              <strong>Operación</strong>: {order.orderData.operationType} <br />
              <strong>Recibes:</strong> {order.orderData.ammount} {order.orderData.currency1} <br/>
              <strong>Envías</strong> {order.orderData.currency2}
            </p>
          );
        }
      }
      
      return (
        <Card
          key={order.id}
          header={order.id}
          status={order.status.state}
          button={<CardButton clicked={() => this.orderDetailsHandler(order.id)}>DETALLES</CardButton>}>
          {content}
        </Card>
      )
    })
  }

  render() {
    let orders = null;
    let panel = null;

    if (this.state.loading) {

      orders = <Spinner />

    } else {

      if (this.state.orders.length === 0) {

        const filter = this.state.filter;

        if (filter === 'on-hold') {
          panel = <Panel>No hay pedidos en espera</Panel>
        }
        if (filter === 'processing') {
          panel = <Panel>No hay pedidos en proceso</Panel>
        }
        if (filter === 'completed') {
          panel = <Panel>No hay pedidos completados</Panel>
        }
        if (filter === 'all') {
          panel = <Panel>No hay pedidos</Panel>
        }

      } else {

        orders = this.listOrders();

      }
    }

    let filterForm = this.state.filterForm;

    let modalContent = this.modalContentHandler();

    return (
      <Aux>
        <Modal title={modalContent.title} show={this.state.showModal} modalClosed={this.showModalHandler}>
          {modalContent.body}
        </Modal>
        <Header>Pedidos</Header>
        <div className={classes.Orders}>
          <Input
            elementType={filterForm.orderSatate.elementType}
            elementConfig={filterForm.orderSatate.elementConfig}
            elementClasses={filterForm.orderSatate.elementClasses}
            value={filterForm.orderSatate.value}
            invalid={!filterForm.orderSatate.valid}
            shouldValidate={filterForm.orderSatate.validation}
            touched={filterForm.orderSatate.touched}
            label={filterForm.orderSatate.label}
            changed={(event) => this.filterOrdersHandler(event, 'orderSatate')} />
          {panel}
          {orders}
        </div>
      </Aux>
    )
  }
}

export default Orders;