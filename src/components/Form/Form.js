import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { auth, db } from '../../lib/firebase';
import FontAwesomeIcon from '@fortawesome/react-fontawesome'

import classes from './Form.css';
import Aux from '../../hoc/Aux/Aux';
import Input from '../UI/Input/Input';
import Button from '../UI/Button/Button';
import AddAccount from './AddAccount/AddAccount';
import AgencyRequest from './AgencyRequest/AgencyRequest';
import ChangePassword from './ChangePassword/ChangePassword';
import ForgotPassword from './ForgotPassword/ForgotPassword';
import Login from './Login/Login';

class Form extends Component {
  state = {
    formInputs: {},
    formIsValid: false,
    loadingForm: false,
    errorMessage: ''
  }

  componentWillMount () {
    if (this.props.type === 'AddAccount') {
      this.setState({formInputs: AddAccount})
    }

    if (this.props.type === 'AgencyRequest') {
      this.setState({formInputs: AgencyRequest})
    }
    
    if (this.props.type === 'ChangePassword') {
      this.setState({formInputs: ChangePassword})
    }

    if (this.props.type === 'ForgotPassword') {
      this.setState({formInputs: ForgotPassword})
    }

    if (this.props.type === 'Login') {
      this.setState({formInputs: Login})
    }
  }

  componentDidMount() {
    if (this.props.values && this.state.formInputs) {
      const formData = {
        ...this.state.formInputs
      }
      for (const key in formData) {
        formData[key].value = this.props.values[key]
      }
      this.setState({formInputs: formData})
    }
  }

  checkValidity (value, rules) {
    let isValid = true;
    let errorMessage = '';

    if (rules.required) {
      isValid = value.trim() !== '' && isValid;
      errorMessage = isValid ? '' : 'Campo requerido';
    }

    if (rules.minLength) {
      isValid = value.length <= rules.minLength && isValid;
      errorMessage = isValid ? '' :  'Muy corto';
    }

    if (rules.maxLength) {
      isValid = value.length >= rules.maxLength && isValid;
      errorMessage = isValid ? '' :  'Muy largo';
    }

    if (rules.equals) {
      const form = { ...this.state.formInputs }
      const comparedInput = { ...form[rules.equals] }
      const originalInput = { ...form[comparedInput.validation.equals] }

      isValid = value === comparedInput.value && isValid;

      comparedInput.valid = isValid;
      originalInput.valid = isValid;

      form[rules.equals] = comparedInput;
      form[comparedInput.validation.equals] = originalInput;

      this.setState({signupForm: form});
      errorMessage = isValid ? '' :  'Las contraseñas no coinciden';      
    }

    if (rules.type === 'email') {
      const regex = /^[_A-Za-z0-9-+]+(\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\.[A-Za-z0-9]+)*(\.[A-Za-z]{2,})$/g;
      isValid = regex.exec(value) && isValid;
      errorMessage = isValid ? '' :  'No es un email válido';      
    }

    return {isValid: isValid, errorMessage: errorMessage};
  }

  inputChangedHandler = (event, inputIdentifier) => {
    const updatedOrderForm = {
      ...this.state.formInputs
    }
    const updatedFormElement = {
      ...updatedOrderForm[inputIdentifier]
    }

    updatedFormElement.value = event.target.value;
    const validity = this.checkValidity(updatedFormElement.value, updatedFormElement.validation);
    updatedFormElement.valid = validity.isValid;
    updatedFormElement.errorMessage = validity.errorMessage;
    updatedFormElement.touched = true;
    updatedOrderForm[inputIdentifier] = updatedFormElement;

    let formIsValid = true;
    for (let inputIdentifier in updatedOrderForm) {
      formIsValid = updatedOrderForm[inputIdentifier].valid && formIsValid;
    }

    this.setState({formInputs: updatedOrderForm, formIsValid: formIsValid});
  }

  submitFormHandler = event => {
    event.preventDefault();
    this.setState({loadingForm: true});

    const formData = {
      ...this.state.formInputs
    }

    const type = this.props.type;

    if (type === 'AddAccount') {
      for (const formElementIdentifier in this.state.formInputs) {
        formData[formElementIdentifier] = this.state.formInputs[formElementIdentifier].value;
      }
      const user = auth.currentUser;
      const newAccount = this.props.values ? this.props.values.id : db.ref(`/users/${user.uid}/accounts`).push().key;

      const account = {
        ...formData
      }

      let updates = {};
      updates[`/users/${user.uid}/accounts/${newAccount}`] = account;
      db.ref().update(updates).then(() => {
        console.log('[AddAccount] Added/Edited')
        this.setState({loadingForm: false})
      }).catch(error => {
        console.log('[AddAccount] Error', error)
      })

    }
    
    if (type === 'AgencyRequest') {
      const email = formData.email.value,
      password = formData.password.value;
      
      auth.createUserWithEmailAndPassword(email, password).catch(error => {
        this.setState({loadingForm: false});
        console.log(error.code, error.message);
      });

      auth.onAuthStateChanged(user => {
        if (user) {
          this.writeAgencyData(user.uid, formData)
          
          user.updateProfile({
            displayName: formData.displayName.value,
            phone: formData.phone.value
          }).then(() => {
            console.log('[updateProfile] Updated')
          }).catch(error => {
            console.log('[updateProfile] Error')
          })

          user.sendEmailVerification().then(() => {
            console.log('Email sent');
          }).catch(error => {
            console.log('Error');
          })

          this.props.history.replace('/verify-email')
        } else {
          console.log('[onAuthStateChanged] error')
        }
      });
    }

    if (type === 'ChangePassword') {
      const user = auth.currentUser,
      newPassword = formData.newPassword;

      user.updatePassword(newPassword).then(() => {
        console.log('[updatePassword] Updated');
      }).catch(error => {
        console.log('[updatePassword] Error', error);
      })
    }

    if (type === 'ForgotPassword') {
      const emailAddress = formData.email.value;

      auth.sendPasswordResetEmail(emailAddress).then(() => {
        this.setState({loadingForm: false});
        console.log('[sendPasswordResetEmail] Enviado');
      }).catch(error => {
        console.log('[sendPasswordResetEmail] Error');
      })
    }

    if (type === 'Login') {
      const email = formData.email.value,
      password = formData.password.value;

      auth.signInWithEmailAndPassword(email, password).then(() => {
        this.setState({loadingForm: false})
      }).catch(error => {
        if (error.code) {
          console.log(error.code, error.message)
          this.setState({loadingForm: false, errorMessage: error.message})
        }
      })

      auth.onAuthStateChanged(user => {
        if (user) {
          if (!user.emailVerified) {
            this.props.history.replace('/verify-email')
          } else {
            const query = new URLSearchParams(this.props.location.search);

            let queryString = [];
            let pathname = '';
            for (let param of query.entries()) {
              if (param[0] === 'redirect_url') {
                pathname = `/${param[1]}`
              } else {
                queryString = [
                  ...queryString,
                  `${param[0]}=${param[1]}`
                ]
              }
            }
            this.props.history.push({
              pathname: pathname,
              search: `?${queryString.join('&')}`
            });
          }
        }
      })
    }
  }

  writeAgencyData = (userId, formData) => {
    db.ref('agencys/' + userId).set({
      data: {
        ruc: formData.ruc.value,
        corporateName: formData.corporateName.value,
        email: formData.email.value,
        country: formData.country.value,
        department: formData.department.value,
        province: formData.province.value,
        district: formData.district.value,
        address: formData.address.value
      },
      approved: false,
      rating: 0,
      registered: new Date()
    });
  }

  render () {
    let inputs = [];
    for (let key in this.state.formInputs) {
      inputs = [
        ...inputs,
        {
          id: key,
          config: this.state.formInputs[key]
        }
      ]
    }

    let loginLinks = null;

    if (this.props.type === 'Login') {
      loginLinks = (
        <Aux>
          <p>¿No eres miembro? <span className={classes.RegisterLink}><Link to="/signup">Regístrate</Link></span></p>
          <br/>
          <p className={classes.RegisterLink}><Link to="/forgot-password">¿Has olvidado tu contraseña?</Link></p>
        </Aux>
      )
    }
    
    const form = (
      <Aux>
        <form onSubmit={this.submitFormHandler} autoComplete="on">   
          {this.state.errorMessage}   
          {inputs.map(input => (
            <Input
              key={input.id}
              elementType={input.config.elementType}
              elementConfig={input.config.elementConfig}
              elementClasses={input.config.elementClasses}
              value={input.config.value}
              invalid={!input.config.valid}
              shouldValidate={input.config.validation}
              touched={input.config.touched}
              label={input.config.label}
              errorMessage={input.config.errorMessage}
              changed={(event) => this.inputChangedHandler(event, input.id)} />
          ))}
          <br/>
          <Button 
            btnType="Success"
            clicked={this.submitFormHandler}>{this.state.loadingForm ? <FontAwesomeIcon icon="circle-notch" spin /> : this.props.submitButton}</Button>
        </form>
        {loginLinks}
      </Aux>
    )

    return form
  }
}

export default withRouter(Form)