import React from 'react';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';

import classes from './SmallCard.css';

const feature = (props) => {
  const icon = props.icon ? <div className={classes.Icon}><FontAwesomeIcon icon={props.icon} /></div> : null;
  return (
    <div className={classes.SmallCard}>
      {icon}
      <div className={classes.Info}>
        <h3>{props.title}</h3>
        <p>{props.children}</p>
      </div>
    </div>
  )
}
export default feature;