import React from 'react';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';

import classes from './Footer.css';
import FooterColumn from './FooterColumn/FooterColumn'

const footer = () => (
  <footer className={classes.Footer}>
    <FooterColumn title="Ayuda">
      <p>¿Quienes somos?</p>
      <p>FAQ</p>
      <p>Monedas</p>
      <p>Países</p>
    </FooterColumn>
    <FooterColumn title="Contacto">
      <a className={[classes.Icon, classes.Contact].join(' ')} href="https://api.whatsapp.com/send?phone=51948837225" target="_blank" rel="noopener noreferrer">
          <FontAwesomeIcon icon={["fab","whatsapp"]} />
          <span>+51 948 837 225</span>
      </a>
      <a className={[classes.Icon, classes.Contact].join(' ')} href="tel:+515555555">+515555555</a>
      <a className={[classes.Icon, classes.Contact].join(' ')} href="mailto:info@sav.io">info@sav.io</a>
    </FooterColumn>
    <FooterColumn title="Redes sociales">
      <a className={classes.Icon} href="https://facebook.com/cambista.online" target="_blank" rel="noopener noreferrer">
        <FontAwesomeIcon icon={["fab","facebook-square"]} />
      </a>
      <a className={classes.Icon} href="https://twitter.com/cambistaonline_" target="_blank" rel="noopener noreferrer">
        <FontAwesomeIcon icon={["fab","twitter-square"]} />
      </a>
      <a className={classes.Icon} href="https://www.instagram.com/cambista.online/" target="_blank" rel="noopener noreferrer">
        <FontAwesomeIcon icon={["fab","instagram"]} />
      </a>
    </FooterColumn>
    <div className={classes.Bottom}>
      Todos los derechos reservados &copy;SAVIO 2018
    </div>
  </footer>
);

export default footer;