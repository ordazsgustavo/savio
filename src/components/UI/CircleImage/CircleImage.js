import React from 'react';

import classes from './CircleImage.css';

const circleImage = (props) => (
  <div className={classes.CircleImage}>
    <img src={props.src} alt={props.alt} />
  </div>
)

export default circleImage;