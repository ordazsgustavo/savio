import React from 'react';

import classes from './Panel.css';

const panel = (props) => {
  let styles = [classes.PanelContent];
  if (props.centered) {
    styles.push(classes.Centered)
  }
  return (
    <div className={classes.Panel}>
      <div className={styles.join(' ')}>
        {props.children}
      </div>
    </div>
  )
}
export default panel;