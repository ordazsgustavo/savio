import React from 'react';

import classes from './Table.css';

const table = (props) => {
  let headers = [];
  for (let index = 0; index < props.headers.length; index++) {
    headers[index] = <td>{props.headers[index]}</td>;
  }

  let rows = [];
  for (let index = 0; index < props.rows.length; index++) {
    rows[index] = <td>{props.rows[index]}</td>;
  }

  return (
    <table className={classes.Table}>
        <thead>
          <tr>
            {headers}
          </tr>
        </thead>
        <tbody>
          <tr>
            {rows}
          </tr>
        </tbody>
        <tfoot>
          <tr>
            {headers}
          </tr>
        </tfoot>
      </table>
  )
}

export default table;