import React from 'react';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';

import classes from './BackToTop.css';

const backToTop = (props) => {

  const clicked = () => {
    window.scroll({
      top: 0, 
      behavior: 'smooth' 
    })
  }

  return <button className={classes.BackToTop} onClick={clicked}><FontAwesomeIcon icon="arrow-up" /></button>
}

export default backToTop;