import React, { Component } from 'react';

import classes from './Modal.css';
import Aux from '../../../hoc/Aux/Aux';
import Backdrop from '../Backdrop/Backdrop';

class Modal extends Component {
  shouldComponentUpdate(nextProps, nextState) {
    return nextProps.show !== this.props.show || nextProps.children !== this.props.children;
  }

  componentDidMount() {
    document.body.style.overflowY = this.props.show ? 'hidden' : 'auto'
  }
  componentWillReceiveProps(nextProps) {
    document.body.style.overflowY = nextProps.show ? 'hidden' : 'auto'
  }
  componentWillUnmount() {
    document.body.style.overflow = 'auto'
  }

  render () {
    let stylesContent = [classes.Content];
    if (this.props.centered) {
      stylesContent = [...stylesContent, classes.Centered];
    }
    return (
      <Aux>
        <Backdrop show={this.props.show} clicked={this.props.modalClosed} />
        <div 
          className={classes.Modal}
          style={{
            transform: this.props.show ? 'translateY(0)' : 'translateY(-100vh)',
            opacity: this.props.show ? '1' : '0'
          }}>
          <div className={classes.Header}>
            <h3>{this.props.title}</h3>
          </div>
          <div className={stylesContent.join(' ')}>
            {this.props.children}
          </div>
        </div>
      </Aux>
    );
  }
}

export default Modal;