import React, { Component } from 'react';
import { auth, db } from '../../../lib/firebase';

import classes from './Rates.css';
import Aux from '../../../hoc/Aux/Aux';
import Header from '../../../components/UI/Header/Header';
import Modal from '../../../components/UI/Modal/Modal';
import Panel from '../../../components/UI/Panel/Panel';
import Input from '../../../components/UI/Input/Input';
import Button from '../../../components/UI/Button/Button';
import Badge from '../../../components/UI/Badge/Badge';

class Rates extends Component {
  state = {
    userId: '',
    rates: [],
    showModal: false,
    addRateForm: {
      from: {
        elementType: 'select',
        elementConfig: {
          options: [
            {value: 'usd', displayValue: 'USD'},
            {value: 'pen', displayValue: 'PEN'},
            {value: 'eur', displayValue: 'EUR'},
            {value: 'btc', displayValue: 'BTC'}
          ]
        },
        elementClasses: ['Half'],
        value: 'usd',
        validation: {},
        valid: true
      },
      to: {
        elementType: 'select',
        elementConfig: {
          options: [
            {value: 'usd', displayValue: 'USD'},
            {value: 'pen', displayValue: 'PEN'},
            {value: 'eur', displayValue: 'EUR'},
            {value: 'btc', displayValue: 'BTC'}
          ]
        },
        elementClasses: ['Half'],
        value: 'pen',
        validation: {},
        valid: true
      },
      rate: {
        elementType: 'input', 
        elementConfig: {
          type: 'tel'
        },
        value: '',
        validation: {
          required: true
        },
        valid: false,
        touched: false,
        label: 'Tipo de cambio'
      }
    }
  }

  componentWillMount() {
    const currentUserID = auth.currentUser.uid;
    this.setState({userId: currentUserID});

    const ratesRef = db.ref(`/agencys/${currentUserID}/rates`);

    let rates = []

    ratesRef.on('child_added', snap => {
      if (snap.val()) {
        rates = [
          ...rates,
          {
            ...snap.val(),
            id: snap.key
          }
        ];
  
        this.setState({rates: rates});
      }
    });
  }

  checkValidity (value, rules) {
    let isValid = true;

    if (rules.required) {
      isValid = value.trim() !== '' && isValid;
    }

    if (rules.minLength) {
      isValid = value.length >= rules.minLength && isValid;
    }

    if (rules.maxLength) {
      isValid = value.length >= rules.minLength && isValid;
    }

    return isValid;
  }

  inputChangedHandler = (event, inputIdentifier) => {
    const updatedOrderForm = {
      ...this.state.addRateForm
    }
    const updatedFormElement = {
      ...updatedOrderForm[inputIdentifier]
    }

    updatedFormElement.value = event.target.value;
    updatedFormElement.valid = this.checkValidity(updatedFormElement.value, updatedFormElement.validation);
    updatedFormElement.touched = true;
    updatedOrderForm[inputIdentifier] = updatedFormElement;

    let formIsValid = true;
    for (let inputIdentifier in updatedOrderForm) {
      formIsValid = updatedOrderForm[inputIdentifier].valid && formIsValid;
    }

    this.setState({addRateForm: updatedOrderForm, formIsValid: formIsValid});
  }

  addRateHandler = (event) => {
    event.preventDefault();
    this.setState({loading: true});
    const formData = {};
    const user = this.state.userId;

    for (const formElementIdentifier in this.state.addRateForm) {
      formData[formElementIdentifier] = this.state.addRateForm[formElementIdentifier].value;
    }

    // const newRate = db.ref(`/agencys/${user}/rates`).push().key;

    const rate = formData.rate

    let updates = {};
    updates[`/agencys/${user}/rates/${formData.from}2${formData.to}`] = rate;
    db.ref().update(updates);
    
    this.setState({showModal: !this.state.showModal});
  }

  showModalHandler = () => {
    this.setState({showModal: !this.state.showModal})
  }

  rateDetailsHandler = () => {
    alert('Detalles');
  }

  deleteRateHandler = () => {
    alert('Deleted');
  }

  render () {
    let panel = null;
    let badges = null;
    if (this.state.rates.length === 0) {
      panel = (
        <Panel>No has agregado tipos de cambio todavia</Panel>
      );
    } else {
      badges = (
        this.state.rates.map(rate => {
          const rates = rate.id.split('2')
          return (
            <Badge
              key={rate.id}
              clicked={this.rateDetailsHandler}
              deleted={this.deleteRateHandler}>
              {rates[0]} -> {rates[1]}
            </Badge>
          )}
        )
      );
    }

    const formElementsArray = [];
    for (let key in this.state.addRateForm) {
      formElementsArray.push({
        id: key,
        config: this.state.addRateForm[key]
      });
    }
    let form = null;

    form = (
      <form onSubmit={this.addRateHandler}>      
        {formElementsArray.map(formElement => (
          <Input
            key={formElement.id}
            elementType={formElement.config.elementType}
            elementConfig={formElement.config.elementConfig}
            elementClasses={formElement.config.elementClasses}
            value={formElement.config.value}
            invalid={!formElement.config.valid}
            shouldValidate={formElement.config.validation}
            touched={formElement.config.touched}
            label={formElement.config.label}
            changed={(event) => this.inputChangedHandler(event, formElement.id)} />
        ))}
        <div className={classes.Buttons}>
          <Button 
            btnType="Success" 
            disabled={!this.state.formIsValid}
            clicked={this.addRateHandler}>Agregar</Button>
        </div>
      </form>
    );

    return (
      <Aux>
        <Modal 
          centered
          title="Agregar cambio"
          show={this.state.showModal} 
          modalClosed={this.showModalHandler}>
          {form}
        </Modal>
        <Header>Cambio del día</Header>
        <div className={classes.Rates}>
          {panel}
          <div className={classes.RatesContainer}>
            {badges}
          </div>
          <Button
            btnType="Success"
            clicked={this.showModalHandler}>AGREGAR CAMBIO</Button>
        </div>
      </Aux>
    )
  }
}

export default Rates;