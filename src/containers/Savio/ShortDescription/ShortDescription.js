import React from 'react';

import classes from './ShortDescription.css';

const shortDescription = () => (
  <div className={classes.ShortDescription}>
    <p>¡Realiza tus operaciones de cambio mediante un innovador sistema P2P!</p>
  </div>
)

export default shortDescription;