import React, { Component } from 'react';
import { auth, db } from '../../../lib/firebase';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';

import classes from './NavigationItems.css';
import Aux from '../../../hoc/Aux/Aux';
import NavigationItem from './NavigationItem/NavigationItem';
import Notifications from '../../Notifications/Notifications';

class NavigationItems extends Component {
  state = {
    userType: '',
    approved: false,
    name: '',
    showSubmenu: false,
    showNotifications: false,
    loading: true
  }

  // shouldComponentUpdate(nextProps, nextState) {
  //   return nextState.userId !== this.state.userId;
  // }

  componentWillMount () { 
    auth.onAuthStateChanged(user => {
      if (user) {

        db.ref('/users/' + user.uid).once('value').then(snap => {
          if (snap.val()) {
            this.setState({userType: 'user', name: user.displayName, loading: false});
          }
        })

        db.ref('/agencys/' + user.uid).once('value').then(snap => {
          if (snap.val()) {
            this.setState({userType: 'agency', name: user.displayName, approved: snap.val().approved, loading: false});
          }
        })

        db.ref('/admins/' + user.uid).once('value').then(snap => {
          if (snap.val()) {
            this.setState({userType: 'admin', name: user.displayName,  loading: false});
          }
        })
      } else {
        this.setState({userType: '', name: '',  loading: false})
      }
    });
  }

  showSubmenuHandler = (e) => {
    if (!this.state.showSubmenu) {
      e.stopPropagation();
    }
    this.setState({showSubmenu: !this.state.showSubmenu});
  }

  showNotificationsHandler = (e) => {
    if (!this.state.showNotifications) {
      e.stopPropagation();
    }
    this.setState({showNotifications: !this.state.showNotifications});
  }

  render () {
    let navLinks = null;
    
    if (!this.state.loading) {
      const userType = this.state.userType;

      if (userType === 'user') {
        navLinks = (
          <Aux>
            <NavigationItem link="/" icon="home" exact>Inicio</NavigationItem>
            <NavigationItem link="/agencys" icon="building">Agencias</NavigationItem>
            <li className={classes.SubmenuWrapper}> <FontAwesomeIcon icon="user" /> <span onClick={this.showSubmenuHandler}>Hola, {this.state.name} <FontAwesomeIcon icon="caret-down" /></span> 
              <div style={{display: this.state.showSubmenu ? 'block' : 'none'}} className={classes.ArrowUp}></div>
              <ul style={{display: this.state.showSubmenu ? 'block' : 'none'}} className={classes.Submenu}>
                <NavigationItem clicked={this.showSubmenuHandler} link="/profile">Perfil</NavigationItem>
                <NavigationItem clicked={this.showSubmenuHandler} link="/orders">Pedidos</NavigationItem>
                <NavigationItem clicked={this.showSubmenuHandler} link="/accounts">Mis cuentas</NavigationItem>
                <NavigationItem clicked={this.showSubmenuHandler} link="/logout" icon="sign-out-alt">Salir</NavigationItem>
              </ul>
            </li>
            <div className={classes.NotificationMenu}>
              <span onClick={this.showNotificationsHandler}><FontAwesomeIcon icon="bell" /></span>
              <Notifications display={this.state.showNotifications} />
            </div>
          </Aux>
        )
      } 

      if (userType === 'agency') {
        if (this.state.approved) {
          navLinks = (
            <Aux>
              <NavigationItem link="/" icon="home" exact>Inicio</NavigationItem>
              <NavigationItem link="/agencys" icon="building">Agencias</NavigationItem>
              <li className={classes.SubmenuWrapper}><span onClick={this.showSubmenuHandler}>Hola, {this.state.name} <FontAwesomeIcon icon="caret-down" /></span> 
                <ul style={{display: this.state.showSubmenu ? 'block' : 'none'}} className={classes.Submenu}>
                  <NavigationItem clicked={this.showSubmenuHandler} link="/profile">Perfil</NavigationItem>
                  <NavigationItem clicked={this.showSubmenuHandler} link="/orders">Pedidos</NavigationItem>
                  <NavigationItem clicked={this.showSubmenuHandler} link="/accounts">Mis cuentas</NavigationItem>
                  <NavigationItem clicked={this.showSubmenuHandler} link="/rates">Tipo de cambio</NavigationItem>
                  <NavigationItem clicked={this.showSubmenuHandler} link="/logout" icon="sign-out-alt">Salir</NavigationItem>
                </ul>
              </li>
              <div className={classes.NotificationMenu}>
                <span onClick={this.showNotificationsHandler}><FontAwesomeIcon icon="bell" /></span>
                <Notifications display={this.state.showNotifications} />
              </div>
            </Aux>
          );
        } else {
          navLinks = (
            <Aux>
              <NavigationItem link="/" icon="home" exact>Inicio</NavigationItem>
              <li className={classes.SubmenuWrapper}><span onClick={this.showSubmenuHandler}><FontAwesomeIcon icon="user" /> Hola, {this.state.name} <FontAwesomeIcon icon="caret-down" /></span> 
                <ul style={{display: this.state.showSubmenu ? 'block' : 'none'}} className={classes.Submenu}>
                  <NavigationItem clicked={this.showSubmenuHandler} link="/profile">Perfil</NavigationItem>
                  <NavigationItem clicked={this.showSubmenuHandler} link="/logout" icon ="sign-out-alt">Salir</NavigationItem>
                </ul>
              </li>
            </Aux>
          )
        }
        
      }

      if (userType === 'admin') {
        navLinks = (
          <Aux>
            <NavigationItem link="/" icon="home" exact>Inicio</NavigationItem>
            <NavigationItem link="/agencys" icon="building">Agencias</NavigationItem>
            <li className={classes.SubmenuWrapper}><span onClick={this.showSubmenuHandler}>Hola, {this.state.name} <FontAwesomeIcon icon="caret-down" /></span> 
              <ul style={{display: this.state.showSubmenu ? 'block' : 'none'}} className={classes.Submenu}>
                <NavigationItem clicked={this.showSubmenuHandler} link="/dashboard">Dashboard</NavigationItem>
                <NavigationItem clicked={this.showSubmenuHandler} link="/logout"> <FontAwesomeIcon icon="sign-out-alt" /> Salir</NavigationItem>
              </ul>
            </li>
            <div className={classes.NotificationMenu}>
              <span onClick={this.showNotificationsHandler}><FontAwesomeIcon icon="bell" /></span>
              <Notifications display={this.state.showNotifications} />
            </div>
          </Aux>
        )
      }

      if (userType === '') {
        navLinks = (
          <Aux>
            <NavigationItem link="/" icon="home" exact>Inicio</NavigationItem>
            <NavigationItem link="/agencys" icon="building">Agencias</NavigationItem>
            <NavigationItem link="/login" icon="sign-in-alt">Ingresar</NavigationItem>
            <NavigationItem link="/signup" icon="user-plus">Registrarse</NavigationItem>
          </Aux>
        )
      }
      
    } 

    return (
      <ul className={classes.NavigationItems} onClick={this.props.clicked}>
        {navLinks}
      </ul>
    )
  }
}

export default NavigationItems;