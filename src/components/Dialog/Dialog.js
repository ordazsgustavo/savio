import React, { Component } from 'react';
import { auth } from '../../lib/firebase';

import classes from './Dialog.css';
import DialogContent from './DialogContent/DialogContent';
import Button from '../UI/Button/Button';
import Spinner from '../UI/Spinner/Spinner';
import Input from '../UI/Input/Input';

const SCALE = [
  'pen',
  'usd',
  'eur',
  'eth',
  'btc'
]

class Dialog extends Component {
  state = {
    accounts: [],
    dialog: {
      operation: {
        elementType: 'radio',
        elementConfig: {
          type: 'radio',
          name: 'operation',
          radios: [
            {value: 'want', label: 'Quiero'},
            {value: 'have', label: 'Tengo'}
          ]
        },
        value: 'want',
        validation: {},
        valid: true
      },
      ammount: { 
        elementType: 'input', 
        elementConfig: {
          type: 'tel'
        },
        elementClasses: ['ThreeThird'],
        value: '',
        validation: {
          required: true
        },
        valid: false,
        touched: false,
        label: 'Monto'
      },
      currency1: {
        elementType: 'select',
        elementConfig: {
          options: [
            {value: 'usd', displayValue: 'USD'},
            {value: 'pen', displayValue: 'PEN'},
            {value: 'btc', displayValue: 'BTC'}
          ]
        },
        elementClasses: ['Third'],
        value: 'usd',
        validation: {},
        valid: true
      }
    },
    answer: [
      '¿Que operación deseas realizar?'
    ],
    formIsValid: false,
    loading: false,
    step: 0
  }

  // orderHandler = event => {
  //   event.preventDefault();
  //   this.setState({loading: true});
  //   const formData = {};
    
  //   let user = auth.currentUser;

  //   for (const formElementIdentifier in this.state.dialog) {
  //     formData[formElementIdentifier] = this.state.dialog[formElementIdentifier].value;
  //   }

  //   if (user) {
  //     // User is signed in.);
  //     const newOrder = db.ref('orders').push().key;
  //     const newNotification = db.ref('notifications/').push().key;

  //     const userData = db.ref(`users/${user.uid}`);
  //     let order = {};

  //     userData.on('value', snap => {
  //       const generated = new Date();
  //       order = {
  //         orderData: {
  //           ...formData,
  //           operationType: this.operationType(formData.operation, formData.currency1, formData.currency2)
  //         },
  //         status: {
  //           state: 'on-hold',
  //           steps: {
  //             generated: generated
  //           }
  //         },
  //         user: {
  //           id: user.uid,
  //           name: snap.val().personalData.name,
  //           lastName: snap.val().personalData.lastName,
  //           docType: snap.val().personalData.docType,
  //           document: snap.val().personalData.document
  //         }
  //       }

  //       const userNotification = {
  //         generated: generated,
  //         content: `Has generado un nuevo pedido ${newOrder}`,
  //         linkTo: `/order/details?id=${newOrder}`
  //       }

  //       const agencyNotification = {
  //         generated: generated,
  //         content: `Nuevo pedido`,
  //         linkTo: `/order/details?id=${newOrder}`
  //       }

  //       let updates = {};
  //       updates[`/orders/${newOrder}`] = order;
  //       updates[`/notifications/users/${user.uid}/${newNotification}`] = userNotification;
  //       updates[`/notifications/agencys/general/${newNotification}`] = agencyNotification;
  //       db.ref().update(updates);
  //     });

  //     this.props.push({
  //       pathname: '/orders'
  //     });
      
  //   } else {
  //     // No user is signed in.
  //     console.log('Not signed in'); 
  //   }
  // }

  operationType = (operation, currency1, currency2) => {
    if (currency1 === currency2) {
      return console.log('Error, equal values', currency1, currency2);
    }

    if (currency1 === -1 || currency2 === -1) {
      return console.log('Error, value doesn\'t exists', currency1, currency2);
    }

    if (operation === 'have') {
      if (SCALE.indexOf(currency1) < SCALE.indexOf(currency2)) {
        return 'venta';
      }

      if (SCALE.indexOf(currency1) > SCALE.indexOf(currency2)) {
        return 'compra';
      }
    }

    if (operation === 'want') {
      if (SCALE.indexOf(currency1) < SCALE.indexOf(currency2)) {
        return 'compra';
      }

      if (SCALE.indexOf(currency1) > SCALE.indexOf(currency2)) {
        return 'venta';
      }
    }
  }

  checkValidity (value, rules) {
    let isValid = true;

    if (rules.required) {
      isValid = value.trim() !== '' && isValid;
    }

    if (rules.minLength) {
      isValid = value.length >= rules.minLength && isValid;
    }

    if (rules.maxLength) {
      isValid = value.length >= rules.minLength && isValid;
    }

    return isValid;
  }

  inputChangedHandler = (event, inputIdentifier) => {
    if (inputIdentifier === 'operation') {
      let ammountAnswer;
      let currency2Answer;
      if (event.target.value === 'want') {
        ammountAnswer = '¿Cuánto quieres?';
        currency2Answer = '¿Qué moneda tienes?';
      } else {
        ammountAnswer = '¿Cuánto tienes?';
        currency2Answer = '¿Qué moneda quieres?';
      }

      const updatedAnswers = {
        ...this.state.answer
      }

      updatedAnswers.ammount = ammountAnswer;
      updatedAnswers.currency2 = currency2Answer;

      this.setState({answer: updatedAnswers});
    }

    const updatedOrderForm = {
      ...this.state.dialog
    }
    const updatedFormElement = {
      ...updatedOrderForm[inputIdentifier]
    }

    if (inputIdentifier === 'receivingBank') {
      const accountNumber = {
        ...updatedOrderForm['accountNumber']
      }

      if (this.state.accounts[event.target.value]) {
        accountNumber.value = this.state.accounts[event.target.value].number;
      } else {
        accountNumber.value = '';
      }

      updatedOrderForm.accountNumber = accountNumber;
      this.setState({dialog: updatedOrderForm});
    }    

    updatedFormElement.value = event.target.value;
    updatedFormElement.valid = this.checkValidity(updatedFormElement.value, updatedFormElement.validation);
    updatedFormElement.touched = true;
    updatedOrderForm[inputIdentifier] = updatedFormElement;

    let formIsValid = true;
    for (let inputIdentifier in updatedOrderForm) {
      formIsValid = updatedOrderForm[inputIdentifier].valid && formIsValid;
    }

    this.setState({dialog: updatedOrderForm, formIsValid: formIsValid});
  }

  orderHandler = () => {
    const user = auth.currentUser;

    let queryString = [];

    for (const key in this.state.dialog) {
      queryString = [ ...queryString, `${key}=${this.state.dialog[key].value}`]
    }

    if (!user) {
      console.log('Not logged in');
      this.props.push(`/login?redirect_url=savio-flow&${queryString.join('&')}`)
      return;
    }

    this.props.push(`/savio-flow?${queryString.join('&')}`)
  }

  subStepHandler = () => {
    const step = this.state.step - 1
    this.setState({step: step})
  }

  render () {
    let form = null;

    if (this.state.loading) {
      form = <Spinner />
    } else {
      const formElementsArray = [];
      for (let key in this.state.dialog) {
        formElementsArray.push({
          id: key,
          config: this.state.dialog[key]
        });
      }
      form = formElementsArray.map(element => (
        <Input
          key={element.id}
          elementType={element.config.elementType}
          elementConfig={element.config.elementConfig}
          elementClasses={element.config.elementClasses}
          value={element.config.value}
          invalid={!element.config.valid}
          shouldValidate={element.config.validation}
          touched={element.config.touched}
          label={element.config.label}
          changed={(event) => this.inputChangedHandler(event, element.id)} />
      ))}
    
    return (
      <div className={classes.Dialog}>
        <form onSubmit={this.orderHandler}>
          <DialogContent answer='¿Que operación deseas realizar?'>
            {form}
          </DialogContent>
          
          <div className={classes.Buttons}>
            <Button 
              btnType="Info" 
              linkTo="#">Comparar</Button>
            <Button btnType="Success" clicked={this.addStepHandler}>Siguiente</Button>
          </div>
        </form>
      </div>
    )
  }
}

export default Dialog;