import React from 'react';

import classes from './SavioStepper.css';

const savioStepper = (props) => {
  const total = props.steps.length;
  const current = props.current;

  // const percentaje = (current * 100) / total;
  // const percentaje = total * total * current;
  // const percentaje = (current / total) * 100;
  const percentaje = (100 / (total - 1)) * current;

  let count = 0;
  return (
    <div className={classes.SavioStepper}>
      <div className={classes.Wrapper}>
        <div className={classes.Progress}>
          <div className={classes.Filler}></div>
          <div className={classes.Ending} style={{width: `${percentaje}%`}}></div>
        </div>
        <ul className={classes.Steps}>
          {props.steps.map(step => {
            const li = <li key={step} className={count === current ? classes.Actual : null}>{step}</li>
            count++
            return li
          })}
        </ul>
      </div>
      
    </div>
  )
}

export default savioStepper;