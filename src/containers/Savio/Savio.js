import React, { Component } from 'react';

// import classes from './Savio.css';
import Hero from './Hero/Hero';
import MarketInfo from './MarketInfo/MarketInfo';
import Features from './Features/Features';
import ShortDescription from './ShortDescription/ShortDescription';

class Savio extends Component {
  render () {
    return (
      <div>
        <Hero 
          title="Cambia tu dinero online"
          subtitle="Simple, seguro y rápido." 
          {...this.props.history} />
        <MarketInfo />
        <ShortDescription />
        <Features />
      </div>
    );
  }
}

export default Savio;