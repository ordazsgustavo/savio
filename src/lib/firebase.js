import firebase from 'firebase';

const config = {
  apiKey: "AIzaSyAXXpuW1XZhD23UFjpcxiRXUrTd2dPnOsQ",
  authDomain: "savio-38674.firebaseapp.com",
  databaseURL: "https://savio-38674.firebaseio.com",
  projectId: "savio-38674",
  storageBucket: "savio-38674.appspot.com",
  messagingSenderId: "374954550772"
}

firebase.initializeApp(config)

export const db = firebase.database()
export const auth = firebase.auth()
export const storage = firebase.storage()