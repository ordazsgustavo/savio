import React from 'react';
import { Link } from 'react-router-dom';

import classes from './Button.css';

const button = (props) => {
  let styles = [classes.Button];

  if (props.btnType === 'Unformatted') {
    styles = [classes.Unformatted];
  }

  let button = null;
  if (props.linkTo) {
    styles = [...styles, classes.Link, classes[props.btnType]];
    if (props.disabled) {
      styles = [...styles, classes.Disabled];
    }
    button = props.linkTo === '#' ? <a className={styles.join(' ')} src={props.linkTo} onClick={props.clicked}>{props.children}</a> : <Link className={styles.join(' ')} to={props.linkTo} onClick={props.clicked}>{props.children}</Link>;
  } else {
    styles = [...styles, classes[props.btnType]];
    button = <button onClick={props.clicked} disabled={props.disabled} className={styles.join(' ')}>{props.children}</button>;
  }
  return button
}
export default button;