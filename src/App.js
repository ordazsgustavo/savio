import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import { auth, db } from './lib/firebase';

import fontawesome from '@fortawesome/fontawesome'
import solid from '@fortawesome/fontawesome-free-solid'
import regular from '@fortawesome/fontawesome-free-regular'
import brands from '@fortawesome/fontawesome-free-brands'

import ScrollToTop from './hoc/ScrollToTop/ScrollToTop';
import Layout from './hoc/Layout/Layout';
import Spinner from './components/UI/Spinner/Spinner';
import Savio from './containers/Savio/Savio';
import AdminDashboard from './containers/Admin/Dashboard';
import UserOrders from './containers/User/Orders/Orders';
import AgencyOrders from './containers/Agency/Orders/Orders';
import Signup from './containers/Auth/Signup/Signup';
import Login from './containers/Auth/Login/Login';
import VerifyEmail from './containers/Auth/VerifyEmail/VerifyEmail';
import Logout from './containers/Auth/Logout/Logout';
import ForgotPassword from './containers/Auth/ForgotPassword/ForgotPassword';
import AgencyRequest from './containers/Auth/AgencyRequest/AgencyRequest';
import UserAccounts from './containers/User/Accounts/Accounts';
import AgencyAccounts from './containers/Agency/Accounts/Accounts';
import Rates from './containers/Agency/Rates/Rates';
import Agencys from './containers/User/Agencys/Agencys';
import Notifications from './containers/Notifications/Notifications';
import Profile from './containers/User/Profile/Profile';
import SavioFlow from './containers/SavioFlow/SavioFlow';

fontawesome.library.add(solid, regular, brands);

class App extends Component {
  state = {
    userType: '',
    loading: true
  }

  componentWillMount () {  
    auth.onAuthStateChanged(user => {
      if (user) {

        db.ref(`/users/${user.uid}`).once('value').then(snap => {
          if (snap.val()) {
            this.setState({userType: 'user', loading: false});
          }
        })

        db.ref(`/agencys/${user.uid}`).once('value').then(snap => {
          if (snap.val()) {
            this.setState({userType: 'agency', loading: false});
          }
        })

        db.ref(`/admins/${user.uid}`).once('value').then(snap => {
          if (snap.val()) {
            this.setState({userType: 'admin', loading: false});
          }
        })

      } else {
        this.setState({userType: '', loading: false});
      }
    });
  }

  render() {
    let routes = null;
    if (this.state.loading) {
      routes = <Spinner />;
    } else {
      if (this.state.userType === 'user') {
        routes = (
          <Switch>
            <Route path="/savio-flow" component={SavioFlow} />
            <Route path="/verify-email" component={VerifyEmail} />
            <Route path="/notifications" component={Notifications} />
            <Route path="/profile" component={Profile} />
            <Route path="/orders" component={UserOrders} />
            <Route path="/accounts" component={UserAccounts} /> 
            <Route path="/logout" component={Logout} />
            <Route path="/agencys" component={Agencys} />
            <Route path="/" component={Savio} />
          </Switch>
        )
      }

      if (this.state.userType === 'agency') {
        routes = (
          <Switch>
            <Route path="/verify-email" component={VerifyEmail} />
            <Route path="/notifications" component={Notifications} />
            <Route path="/orders" component={AgencyOrders} /> 
            <Route path="/accounts" component={AgencyAccounts} /> 
            <Route path="/rates" component={Rates} /> 
            <Route path="/logout" component={Logout} />
            <Route path="/agencys" component={Agencys} />
            <Route path="/" component={Savio} />
          </Switch>
        )
      }

      if (this.state.userType === 'admin') {
        routes = (
          <Switch>
            <Route path="/verify-email" component={VerifyEmail} />
            <Route path="/notifications" component={Notifications} />
            <Route path="/dashboard" component={AdminDashboard} />
            <Route path="/logout" component={Logout} />
            <Route path="/" component={Savio} />
          </Switch>
        )
      }

      if (this.state.userType === '') {
        routes = (
          <Switch>
            <Route path="/verify-email" component={VerifyEmail} />
            <Route path="/signup" component={Signup} />
            <Route path="/agency-request" component={AgencyRequest} />
            <Route path="/login" component={Login} />
            <Route path="/forgot-password" component={ForgotPassword} />
            <Route path="/agencys" component={Agencys} />
            <Route path="/" component={Savio} />
          </Switch>
        )
      }
    }

    return (
      <Layout>
        <ScrollToTop>
          {routes}
        </ScrollToTop>
      </Layout>
    );
  }
}

export default App;
