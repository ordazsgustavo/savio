import React from 'react';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';

import classes from './StarRating.css';

const starRating = (props) => (
  <div className={classes.starsContainer}>
    <div className={classes.starsOuter}>
      <FontAwesomeIcon icon={['far', 'star']} />
      <FontAwesomeIcon icon={['far', 'star']} />
      <FontAwesomeIcon icon={['far', 'star']} />
      <FontAwesomeIcon icon={['far', 'star']} />
      <FontAwesomeIcon icon={['far', 'star']} />
      <div className={classes.starsInner} style={{width: `${props.percentage}%`}}>
        <FontAwesomeIcon icon="star" />
        <FontAwesomeIcon icon="star" />
        <FontAwesomeIcon icon="star" />
        <FontAwesomeIcon icon="star" />
        <FontAwesomeIcon icon="star" />
      </div>
    </div>
    <span>{props.rating} / 5</span>
  </div>
)


export default starRating;