import React, { Component } from 'react';
import { auth, db } from '../../../lib/firebase';
import { Link } from 'react-router-dom';

import classes from './Signup.css';
import Aux from '../../../hoc/Aux/Aux';
import Header from '../../../components/UI/Header/Header';
import Input from '../../../components/UI/Input/Input';
import Button from '../../../components/UI/Button/Button';
import Panel from '../../../components/UI/Panel/Panel';
import Spinner from '../../../components/UI/Spinner/Spinner';

class Signup extends Component {
  state = {
    signupForm: {
      userType: {
        elementType: 'radio',
        elementConfig: {
          type: 'radio',
          name: 'operation',
          radios: [
            {value: 'natural', label: 'Natural'},
            {value: 'legal', label: 'Jurídico'}
          ]
        },
        value: 'natural',
        validation: {},
        valid: true,
        label: 'Tipo de usuario'
      },
      email: { 
        elementType: 'input', 
        elementConfig: {
          type: 'email',
          autoComplete: 'username'
        },
        value: '',
        validation: {
          required: true,
          type: 'email'
        },
        valid: false,
        touched: false,
        label: 'Correo',
        errorMessage: ''
      },
      password: { 
        elementType: 'input', 
        elementConfig: {
          type: 'password',
          autoComplete: 'new-password'
        },
        value: '',
        validation: {
          required: true
        },
        valid: true,
        touched: false,
        label: 'Contraseña',
        errorMessage: ''
      }
    },
    formIsValid: false,
    loadingForm: false
  }

  signupHandler = (event) => {
    event.preventDefault();
    this.setState({loadingForm: true});

    const formData = {
      ...this.state.signupForm
    }
    
    let email = formData.email.value;
    let password = formData.password.value;
    
    auth.createUserWithEmailAndPassword(email, password).catch(error => {
      if (error) {
        console.log(error.code, error.message);
        this.setState({loadingForm: false});
      }
    });

    auth.onAuthStateChanged(user => {
      if (user) {
        this.writeUserData(user.uid, formData);

        user.updateProfile({
          displayName: `${formData.name.value} ${formData.lastName.value}`,
          phone: formData.phone.value
        }).then(() => {
          console.log('[updateProfile] Updated');
        }).catch(error => {
          console.log('[updateProfile] Error');
        });

        user.sendEmailVerification().then(() => {
          console.log('Email sent');
        }).catch(error => {
          console.log('Error');
        });
        this.props.history.replace('/verify-email');
      } else {
        console.log('[onAuthStateChanged] error');
      }
    });
  }

  writeUserData = (userId, formData) => {
    db.ref('users/' + userId).set({
      personalData: {
        userType: formData.userType.value,
        docType: formData.docType.value,
        document: formData.document.value,
        ruc: formData.ruc.value,
        company: formData.company.value,
        name: formData.name.value,
        lastName: formData.lastName.value,
        phone: formData.phone.value,
        country: formData.country.value,
        department: formData.department.value,
        province: formData.province.value,
        district: formData.district.value,
        address: formData.address.value,
        birthDate: formData.birthDate.value,
        email: formData.email.value,
      },
      registered: new Date()
    });
  }

  checkValidity (value, rules) {
    let isValid = true;
    let errorMessage = '';

    if (rules.required) {
      isValid = value.trim() !== '' && isValid;
      errorMessage = isValid ? '' : 'Campo requerido';
    }

    if (rules.minLength) {
      isValid = value.length >= rules.minLength && isValid;
      console.log('minLength', isValid);
      errorMessage = isValid ? '' :  'Muy corto';

      if (isValid && rules.maxLength) {
        isValid = value.length <= rules.maxLength && isValid;
        console.log('maxLength', isValid);
        errorMessage = isValid ? '' :  'Muy largo';
      }
    } else {
      if (rules.maxLength) {
        isValid = value.length <= rules.maxLength && isValid;
        console.log('maxLength', isValid);
        errorMessage = isValid ? '' :  'Muy largo';
      }
    }

    if (rules.equals) {
      const form = { ...this.state.signupForm }
      const comparedInput = { ...form[rules.equals] }
      const originalInput = { ...form[comparedInput.validation.equals] }

      isValid = value === comparedInput.value && isValid;

      comparedInput.valid = isValid;
      originalInput.valid = isValid;

      form[rules.equals] = comparedInput;
      form[comparedInput.validation.equals] = originalInput;

      this.setState({signupForm: form});
      errorMessage = isValid ? '' :  'Las contraseñas no coinciden';      
    }

    if (rules.type === 'email') {
      const regex = /^[_A-Za-z0-9-+]+(\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\.[A-Za-z0-9]+)*(\.[A-Za-z]{2,})$/g;
      isValid = regex.exec(value) && isValid;
      errorMessage = isValid ? '' :  'No es un email válido';      
    }

    return {isValid: isValid, errorMessage: errorMessage};
  }

  inputChangedHandler = (event, inputIdentifier) => {
    const updatedOrderForm = {
      ...this.state.signupForm
    }
    const updatedFormElement = {
      ...updatedOrderForm[inputIdentifier]
    }

    updatedFormElement.value = event.target.value;
    const validity = this.checkValidity(updatedFormElement.value, updatedFormElement.validation);
    updatedFormElement.valid = validity.isValid;
    updatedFormElement.errorMessage = validity.errorMessage;
    updatedFormElement.touched = true;
    updatedOrderForm[inputIdentifier] = updatedFormElement;

    let formIsValid = true;
    for (let inputIdentifier in updatedOrderForm) {
      formIsValid = updatedOrderForm[inputIdentifier].valid && formIsValid;
    }

    this.setState({signupForm: updatedOrderForm, formIsValid: formIsValid});
  }

  stepperHandler = () => {
    const formElementsArray = [];
    for (let key in this.state.signupForm) {
      formElementsArray.push({
        id: key,
        config: this.state.signupForm[key]
      });
    }

    let userType = null;

    if (formElementsArray[0].config.value === 'natural') {
      userType = (
        <Input
          elementType={formElementsArray[0].config.elementType}
          elementConfig={formElementsArray[0].config.elementConfig}
          elementClasses={formElementsArray[0].config.elementClasses}
          value={formElementsArray[0].config.value}
          invalid={!formElementsArray[0].config.valid}
          shouldValidate={formElementsArray[0].config.validation}
          touched={formElementsArray[0].config.touched}
          label={formElementsArray[0].config.label}
          errorMessage={formElementsArray[0].config.errorMessage}
          changed={(event) => this.inputChangedHandler(event, formElementsArray[0].id)} />
      )
    } else {
      userType = (
        <Aux>
          <Input
            elementType={formElementsArray[0].config.elementType}
            elementConfig={formElementsArray[0].config.elementConfig}
            elementClasses={formElementsArray[0].config.elementClasses}
            value={formElementsArray[0].config.value}
            invalid={!formElementsArray[0].config.valid}
            shouldValidate={formElementsArray[0].config.validation}
            touched={formElementsArray[0].config.touched}
            label={formElementsArray[0].config.label}
            errorMessage={formElementsArray[0].config.errorMessage}
            changed={(event) => this.inputChangedHandler(event, formElementsArray[0].id)} />
          <Input
            elementType={formElementsArray[1].config.elementType}
            elementConfig={formElementsArray[1].config.elementConfig}
            elementClasses={formElementsArray[1].config.elementClasses}
            value={formElementsArray[1].config.value}
            invalid={!formElementsArray[1].config.valid}
            shouldValidate={formElementsArray[1].config.validation}
            touched={formElementsArray[1].config.touched}
            label={formElementsArray[1].config.label}
            errorMessage={formElementsArray[1].config.errorMessage}
            changed={(event) => this.inputChangedHandler(event, formElementsArray[1].id)} />
          <Input
            elementType={formElementsArray[2].config.elementType}
            elementConfig={formElementsArray[2].config.elementConfig}
            elementClasses={formElementsArray[2].config.elementClasses}
            value={formElementsArray[2].config.value}
            invalid={!formElementsArray[2].config.valid}
            shouldValidate={formElementsArray[2].config.validation}
            touched={formElementsArray[2].config.touched}
            label={formElementsArray[2].config.label}
            errorMessage={formElementsArray[2].config.errorMessage}
            changed={(event) => this.inputChangedHandler(event, formElementsArray[2].id)} />
        </Aux>
      )
    }

    let steps = [];

    steps[0] = (
      <Aux>
        {userType}
        <Input
          elementType={formElementsArray[3].config.elementType}
          elementConfig={formElementsArray[3].config.elementConfig}
          elementClasses={formElementsArray[3].config.elementClasses}
          value={formElementsArray[3].config.value}
          invalid={!formElementsArray[3].config.valid}
          shouldValidate={formElementsArray[3].config.validation}
          touched={formElementsArray[3].config.touched}
          label={formElementsArray[3].config.label}
          errorMessage={formElementsArray[3].config.errorMessage}
          changed={(event) => this.inputChangedHandler(event, formElementsArray[3].id)} />
        <Input
          elementType={formElementsArray[4].config.elementType}
          elementConfig={formElementsArray[4].config.elementConfig}
          elementClasses={formElementsArray[4].config.elementClasses}
          value={formElementsArray[4].config.value}
          invalid={!formElementsArray[4].config.valid}
          shouldValidate={formElementsArray[4].config.validation}
          touched={formElementsArray[4].config.touched}
          label={formElementsArray[4].config.label}
          errorMessage={formElementsArray[4].config.errorMessage}
          changed={(event) => this.inputChangedHandler(event, formElementsArray[4].id)} />
        <Input
          elementType={formElementsArray[5].config.elementType}
          elementConfig={formElementsArray[5].config.elementConfig}
          elementClasses={formElementsArray[5].config.elementClasses}
          value={formElementsArray[5].config.value}
          invalid={!formElementsArray[5].config.valid}
          shouldValidate={formElementsArray[5].config.validation}
          touched={formElementsArray[5].config.touched}
          label={formElementsArray[5].config.label}
          errorMessage={formElementsArray[5].config.errorMessage}
          changed={(event) => this.inputChangedHandler(event, formElementsArray[5].id)} />
        <Input
          elementType={formElementsArray[6].config.elementType}
          elementConfig={formElementsArray[6].config.elementConfig}
          elementClasses={formElementsArray[6].config.elementClasses}
          value={formElementsArray[6].config.value}
          invalid={!formElementsArray[6].config.valid}
          shouldValidate={formElementsArray[6].config.validation}
          touched={formElementsArray[6].config.touched}
          label={formElementsArray[6].config.label}
          errorMessage={formElementsArray[6].config.errorMessage}
          changed={(event) => this.inputChangedHandler(event, formElementsArray[6].id)} />
        <Input
          elementType={formElementsArray[7].config.elementType}
          elementConfig={formElementsArray[7].config.elementConfig}
          elementClasses={formElementsArray[7].config.elementClasses}
          value={formElementsArray[7].config.value}
          invalid={!formElementsArray[7].config.valid}
          shouldValidate={formElementsArray[7].config.validation}
          touched={formElementsArray[7].config.touched}
          label={formElementsArray[7].config.label}
          errorMessage={formElementsArray[7].config.errorMessage}
          changed={(event) => this.inputChangedHandler(event, formElementsArray[7].id)} />
      </Aux>
    )

    steps[1] = (
      <Aux>
        <Input
          elementType={formElementsArray[8].config.elementType}
          elementConfig={formElementsArray[8].config.elementConfig}
          elementClasses={formElementsArray[8].config.elementClasses}
          value={formElementsArray[8].config.value}
          invalid={!formElementsArray[8].config.valid}
          shouldValidate={formElementsArray[8].config.validation}
          touched={formElementsArray[8].config.touched}
          label={formElementsArray[8].config.label}
          errorMessage={formElementsArray[8].config.errorMessage}
          changed={(event) => this.inputChangedHandler(event, formElementsArray[8].id)} />
        <Input
          elementType={formElementsArray[9].config.elementType}
          elementConfig={formElementsArray[9].config.elementConfig}
          elementClasses={formElementsArray[9].config.elementClasses}
          value={formElementsArray[9].config.value}
          invalid={!formElementsArray[9].config.valid}
          shouldValidate={formElementsArray[9].config.validation}
          touched={formElementsArray[9].config.touched}
          label={formElementsArray[9].config.label}
          errorMessage={formElementsArray[9].config.errorMessage}
          changed={(event) => this.inputChangedHandler(event, formElementsArray[9].id)} />
        <Input
          elementType={formElementsArray[10].config.elementType}
          elementConfig={formElementsArray[10].config.elementConfig}
          elementClasses={formElementsArray[10].config.elementClasses}
          value={formElementsArray[10].config.value}
          invalid={!formElementsArray[10].config.valid}
          shouldValidate={formElementsArray[10].config.validation}
          touched={formElementsArray[10].config.touched}
          label={formElementsArray[10].config.label}
          errorMessage={formElementsArray[10].config.errorMessage}
          changed={(event) => this.inputChangedHandler(event, formElementsArray[10].id)} />
        <Input
          elementType={formElementsArray[11].config.elementType}
          elementConfig={formElementsArray[11].config.elementConfig}
          elementClasses={formElementsArray[11].config.elementClasses}
          value={formElementsArray[11].config.value}
          invalid={!formElementsArray[11].config.valid}
          shouldValidate={formElementsArray[11].config.validation}
          touched={formElementsArray[11].config.touched}
          label={formElementsArray[11].config.label}
          errorMessage={formElementsArray[11].config.errorMessage}
          changed={(event) => this.inputChangedHandler(event, formElementsArray[11].id)} />
        <Input
          elementType={formElementsArray[12].config.elementType}
          elementConfig={formElementsArray[12].config.elementConfig}
          elementClasses={formElementsArray[12].config.elementClasses}
          value={formElementsArray[12].config.value}
          invalid={!formElementsArray[12].config.valid}
          shouldValidate={formElementsArray[12].config.validation}
          touched={formElementsArray[12].config.touched}
          label={formElementsArray[12].config.label}
          errorMessage={formElementsArray[12].config.errorMessage}
          changed={(event) => this.inputChangedHandler(event, formElementsArray[12].id)} />
      </Aux>
    )

    steps[2] = (
      <Aux>
        <Input
          elementType={formElementsArray[13].config.elementType}
          elementConfig={formElementsArray[13].config.elementConfig}
          elementClasses={formElementsArray[13].config.elementClasses}
          value={formElementsArray[13].config.value}
          invalid={!formElementsArray[13].config.valid}
          shouldValidate={formElementsArray[13].config.validation}
          touched={formElementsArray[13].config.touched}
          label={formElementsArray[13].config.label}
          errorMessage={formElementsArray[13].config.errorMessage}
          changed={(event) => this.inputChangedHandler(event, formElementsArray[13].id)} />
        <Input
          elementType={formElementsArray[14].config.elementType}
          elementConfig={formElementsArray[14].config.elementConfig}
          elementClasses={formElementsArray[14].config.elementClasses}
          value={formElementsArray[14].config.value}
          invalid={!formElementsArray[14].config.valid}
          shouldValidate={formElementsArray[14].config.validation}
          touched={formElementsArray[14].config.touched}
          label={formElementsArray[14].config.label}
          errorMessage={formElementsArray[14].config.errorMessage}
          changed={(event) => this.inputChangedHandler(event, formElementsArray[14].id)} />
        <Input
          elementType={formElementsArray[15].config.elementType}
          elementConfig={formElementsArray[15].config.elementConfig}
          elementClasses={formElementsArray[15].config.elementClasses}
          value={formElementsArray[15].config.value}
          invalid={!formElementsArray[15].config.valid}
          shouldValidate={formElementsArray[15].config.validation}
          touched={formElementsArray[15].config.touched}
          label={formElementsArray[15].config.label}
          errorMessage={formElementsArray[15].config.errorMessage}
          changed={(event) => this.inputChangedHandler(event, formElementsArray[15].id)} />
        <Input
          elementType={formElementsArray[16].config.elementType}
          elementConfig={formElementsArray[16].config.elementConfig}
          elementClasses={formElementsArray[16].config.elementClasses}
          value={formElementsArray[16].config.value}
          invalid={!formElementsArray[16].config.valid}
          shouldValidate={formElementsArray[16].config.validation}
          touched={formElementsArray[16].config.touched}
          label={formElementsArray[16].config.label}
          errorMessage={formElementsArray[16].config.errorMessage}
          changed={(event) => this.inputChangedHandler(event, formElementsArray[16].id)} />
        <Input
          elementType={formElementsArray[17].config.elementType}
          elementConfig={formElementsArray[17].config.elementConfig}
          elementClasses={formElementsArray[17].config.elementClasses}
          value={formElementsArray[17].config.value}
          invalid={!formElementsArray[17].config.valid}
          shouldValidate={formElementsArray[17].config.validation}
          touched={formElementsArray[17].config.touched}
          label={formElementsArray[17].config.label}
          errorMessage={formElementsArray[17].config.errorMessage}
          changed={(event) => this.inputChangedHandler(event, formElementsArray[17].id)} />
      </Aux>
    )

    return steps
  }

  addStepHandler = () => {
    const step = this.state.step + 1
    this.setState({step: step})
  }

  subStepHandler = () => {
    const step = this.state.step - 1
    this.setState({step: step})
  }

  render () {
    let form = null;

    if (this.state.loading) {

      form = <Spinner />

    } else {

      const formElementsArray = [];
      for (let key in this.state.signupForm) {
        formElementsArray.push({
          id: key,
          config: this.state.signupForm[key]
        });
      }

      form = (
        <form onSubmit={this.signupHandler} autoComplete="on">
          {formElementsArray.map(element => (
            <Input
              key={element.id}
              elementType={element.config.elementType}
              elementConfig={element.config.elementConfig}
              elementClasses={element.config.elementClasses}
              value={element.config.value}
              invalid={!element.config.valid}
              shouldValidate={element.config.validation}
              touched={element.config.touched}
              label={element.config.label}
              errorMessage={element.config.errorMessage}
              changed={(event) => this.inputChangedHandler(event, element.id)} />))}

          <Button 
            btnType="Success"
            clicked={this.signupHandler}>Registrarse</Button>
        </form>
      )
    }

    return (
      <Aux>
        <Header>Registro</Header>
        <div className={classes.Signup}>
          <Panel>
            <p style={{margin: '30px 0'}}>¿Ya estas registrado? <span className={classes.RegisterLink}><Link to="/login">Ingresar</Link></span></p>
            {form}
            <p>Al hacer click en "Registrarse", estas aceptando los <span className={classes.RegisterLink}><Link to="/terms-conditions">Términos y Condiciones</Link></span>.</p>
            <p>¿Eres agencia de cambio? <span className={classes.RegisterLink}><Link to="/agency-request">Solicitar registro</Link></span></p>
          </Panel>
        </div>
      </Aux>
    );
  }
}

export default Signup;