import React from 'react';

import classes from './Input.css';

const input = (props) => {
  let wrapperStyles = [classes.Input];
  let inputElement = null;
  let inputClasses = [classes.InputElement];
  let labelText = props.label;

  if (props.invalid && props.shouldValidate && props.touched) {
    inputClasses.push(classes.Invalid);
  }

  if (props.elementClasses) {
    const count = props.elementClasses.length;
    for (let i = 0; i < count; i++) {
      wrapperStyles = [...wrapperStyles, classes[props.elementClasses[i]]]
    }
  }

  if (props.time) {
    if (props.value <= 4) {
      labelText = 'The value is too low';
    }
    if (props.value >= 5 && props.value <= 30) {
      inputClasses.push(classes.Success);
    }

    if (props.value >= 31 && props.value <= 40) {
      inputClasses.push(classes.Warning);
    }

    if (props.value >= 41) {
      inputClasses.push(classes.Danger);
    }
  }
  let required = 'false';
  if (props.validation) {
    required = props.validation.required ? 'true' : 'false'
  }

  switch (props.elementType) {
    case 'input':
        inputElement = <input 
          className={inputClasses.join(' ')} 
          {...props.elementConfig} 
          value={props.value}
          required={required}
          onChange={props.changed} />;
      break;

    case 'textarea':
        inputElement = <input 
          className={inputClasses.join(' ')} 
          {...props.elementConfig} 
          value={props.value}
          onChange={props.changed} />;
      break;

    case 'select':
        inputElement = (
          <label className={classes.arrow}>
            <select
              className={classes.Select}
              value={props.value}
              onChange={props.changed}>
              {props.elementConfig.options.map(option => (
                <option key={option.value} value={option.value} disabled={option.disabled}>
                  {option.displayValue}
                </option>
              ))}
            </select>
          </label>
        );
      break;

    case 'selectGroup':
        inputElement = (
          <label className={classes.arrow}>
            <select
              className={classes.Select}
              value={props.value}
              onChange={props.changed}>
              <optgroup label="Mis cuentas">
                {props.elementConfig.userAccounts.map(option => (
                  <option key={option.value} value={option.value} disabled={option.disabled}>
                    {option.displayValue}
                  </option>
                ))}
              </optgroup>
              <optgroup label="Otras cuentas">
                {props.elementConfig.options.map(option => (
                  <option key={option.value} value={option.value} disabled={option.disabled}>
                    {option.displayValue}
                  </option>
                ))}
              </optgroup>
            </select>
          </label>
        );
      break;

    case 'radio':
        inputElement = (
          props.elementConfig.radios.map(radio => (
            <label key={radio.value} className={classes.RadioLabel}>
              <input 
              type={props.elementType}
              className={inputClasses.join(' ')} 
              name={props.elementConfig.name}
              value={radio.value}
              checked={props.value === radio.value ? true : false}
              onChange={props.changed} />
              <span>{radio.label}</span>
            </label>
          ))
        );
      break;

      case 'checkbox':
        inputElement = (
          props.elementConfig.checks.map(check => (
            <label key={check.value}>
              <input 
              type={props.elementType}
              className={inputClasses.join(' ')} 
              name={props.elementConfig.name}
              value={check.value}
              onChange={props.changed} />
              {check.label}
            </label>
          ))
        );
        break;

    default:
        inputElement = <input 
        className={inputClasses.join(' ')} 
        {...props.elementConfig} 
        value={props.value}
        onChange={props.changed} />;
      break;
  }

  return (
    <div className={wrapperStyles.join(' ')}>
      {inputElement}
      <span className={classes.Bar}></span>
      <label className={classes.Label}>{labelText} {props.shouldValidate.required ? '*' : ''}</label>
      <span>{props.errorMessage}</span>
    </div>
  );
}

export default input;