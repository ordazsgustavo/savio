const form = {
  document: {
    elementType: 'input', 
    elementConfig: {
      type: 'file',
    },
    value: '',
    validation: {},
    valid: true,
    touched: false,
    label: 'DNI'
  },
  voucher: {
    elementType: 'input', 
    elementConfig: {
      type: 'file',
    },
    value: '',
    validation: {},
    valid: false,
    touched: false,
    label: 'Voucher'
  },
  nationality: {
    elementType: 'input', 
    elementConfig: {
      type: 'text',
      placeholder: 'Nacionalidad'
    },
    value: '',
    validation: {
      required: true
    },
    valid: true,
    touched: false
  },
  publicPerson: {
    elementType: 'radio',
    elementConfig: {
      type: 'radio',
      name: 'publicPerson',
      radios: [
        {value: 'no', label: 'No'},
        {value: 'yes', label: 'Si'}
      ]
    },
    value: 'no',
    validation: {},
    valid: true,
    label: '¿Ha sido persona publica?'
  },
  position: {
    elementType: 'input', 
    elementConfig: {
      type: 'text',
      placeholder: 'Cargo'
    },
    value: '',
    validation: {
      required: true
    },
    valid: true,
    touched: false
  },
  institution: {
    elementType: 'input', 
    elementConfig: {
      type: 'text',
      placeholder: 'Institución'
    },
    value: '',
    validation: {
      required: true
    },
    valid: true,
    touched: false
  },
  operationTo: {
    elementType: 'select',
    elementConfig: {
      options: [
        {value: 'myself', displayValue: 'Mi mismo'},
        {value: 'naturalPerson', displayValue: 'Persona Natural'},
        {value: 'legalPerson', displayValue: 'Persona Jurídica'}
      ]
    },
    value: 'myself',
    validation: {},
    valid: true,
    label: 'Operación en favor de:'
  },
  moneyOrigin: {
    elementType: 'input', 
    elementConfig: {
      type: 'text',
      placeholder: 'Origen dinero'
    },
    value: '',
    validation: {
      required: true
    },
    valid: true,
    touched: false
  },
  name: {
    elementType: 'input', 
    elementConfig: {
      type: 'text',
      placeholder: 'Nombres Persona Natural'
    },
    value: '',
    validation: {
      required: true
    },
    valid: true,
    touched: false
  },
  lastName: {
    elementType: 'input', 
    elementConfig: {
      type: 'text',
      placeholder: 'Apellidod Persona Natural'
    },
    value: '',
    validation: {
      required: true
    },
    valid: true,
    touched: false
  },
  legalDocument: {
    elementType: 'checkbox',
    elementConfig: {
      type: 'chekbox',
      name: 'legalDocument',
      checks: [
        {value: 'yes', label: 'Si posee'}
      ]
    },
    value: '',
    validation: {},
    valid: true,
    label: '¿Posee documento de representación?'
  },
  corporateName: {
    elementType: 'input', 
    elementConfig: {
      type: 'text',
      placeholder: 'Razón social'
    },
    value: '',
    validation: {
      required: true
    },
    valid: true,
    touched: false
  },
  ruc: {
    elementType: 'input', 
    elementConfig: {
      type: 'text',
      placeholder: 'RUC'
    },
    value: '',
    validation: {
      required: true
    },
    valid: true,
    touched: false
  },
  economicActivity: {
    elementType: 'input', 
    elementConfig: {
      type: 'text',
      placeholder: 'Actividad económica'
    },
    value: '',
    validation: {
      required: true
    },
    valid: true,
    touched: false
  },
  address: {
    elementType: 'input', 
    elementConfig: {
      type: 'text',
      placeholder: 'Dirección'
    },
    value: '',
    validation: {
      required: true
    },
    valid: true,
    touched: false
  },
  phone: {
    elementType: 'input', 
    elementConfig: {
      type: 'tel',
      placeholder: 'Teléfono'
    },
    value: '',
    validation: {
      required: true
    },
    valid: true,
    touched: false
  }
}

export default form;