import React, { Component } from 'react';
import { auth } from '../../../lib/firebase';

import Aux from '../../../hoc/Aux/Aux';
import classes from './VerifyEmail.css';
import Modal from '../../../components/UI/Modal/Modal';
import Header from '../../../components/UI/Header/Header';
import Panel from '../../../components/UI/Panel/Panel';
import Button from '../../../components/UI/Button/Button';

class VerifyEmail extends Component {
  state = {
    showModal: false,
    resend: false
  }

  componentWillMount() {
    if (this.props.location.search === '?resend') {
      this.setState({resend: true})
    }
  }

  resendEmailHandler = () => {
    const user = auth.currentUser;

    user.sendEmailVerification().then(() => {
      console.log('Email sent')
    }).catch(error => {
      console.log('Error')
    })
  }

  showModalHandler = () => {
    this.setState({ showModal: !this.state.showModal });
  }

  render () {
    let panelContent = (
      <p>
        Gracias por registrarte en <strong>SAVIO</strong>, te enviamos un correo con un enlace de verificacion. <br/><br/>
        Pueden pasar hasta 20 minutos hasta que recibas el correo. Si despues de ese tiempo no lo has recibido, haz click en <strong>REENVIAR EMAIL</strong>.
      </p>
    )

    if (this.state.resend) {
      panelContent = (
        <p>
          Aun no has verificado tu email, por favor revisa tu bandeja de entrada o de spam si no lo encuentras. En caso de que no hayas recibido el correo de verificacion, haz click en <strong>REENVIAR EMAIL</strong>.
        </p>
      )
    }
    
    return (
      <Aux>
        <Modal title="Email enviado" show={this.state.showModal} modalClosed={this.showModalHandler}>
          Hemos enviado el email nuevamente. Si el inconveniente persiste puedes escribirnos a <a href="mailto:info@sav.io">info@sav.io</a>
        </Modal>
        <Header>Verifica tu email</Header>
        <div className={classes.VerifyEmail}>
          <Panel>
            {panelContent}
            
            <Button btnType="Success" linkTo="/">Volver al inicio</Button>
            <Button btnType="Success" clicked={this.resendEmailHandler}>Reenviar email</Button>
          </Panel>
        </div>
      </Aux>
    )
  }
}

export default VerifyEmail;