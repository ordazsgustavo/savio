const agencyRequest = {
  ruc: { 
    elementType: 'input', 
    elementConfig: {
      type: 'tel'
    },
    elementClasses: ['Half'],
    value: '',
    validation: {
      required: true
    },
    valid: false,
    touched: false,
    label: 'RUC'
  },
  corporateName: { 
    elementType: 'input', 
    elementConfig: {
      type: 'text'
    },
    elementClasses: ['Half'],
    value: '',
    validation: {
      required: true
    },
    valid: false,
    touched: false,
    label: 'Razón social'
  },
  displayName: { 
    elementType: 'input', 
    elementConfig: {
      type: 'text'
    },
    value: '',
    validation: {
      required: true
    },
    valid: false,
    touched: false,
    label: 'Nombre visible'
  },
  country: { 
    elementType: 'input', 
    elementConfig: {
      type: 'text',
      autoComplete: 'shipping country'
    },
    elementClasses: ['Half'],
    value: '',
    validation: {
      required: true
    },
    valid: false,
    touched: false,
    label: 'País',
    errorMessage: ''
  },
  department: { 
    elementType: 'select',
    elementConfig: {
      options: [
        {value: 'Amazonas', displayValue: 'Amazonas'},
        {value: 'Ancash', displayValue: 'Ancash'},
        {value: 'Apurimac', displayValue: 'Apurimac'},
        {value: 'Arequipa', displayValue: 'Arequipa'},
        {value: 'Ayacucho', displayValue: 'Ayacucho'},
        {value: 'Cajamarca', displayValue: 'Cajamarca'},
        {value: 'Callao', displayValue: 'Callao'},
        {value: 'Cusco', displayValue: 'Cusco'},
        {value: 'Huancavelica', displayValue: 'Huancavelica'},
        {value: 'Huanuco', displayValue: 'Huanuco'}, 
        {value: 'Ica', displayValue: 'Ica'},
        {value: 'Junin', displayValue: 'Junin'},
        {value: 'La Libertad', displayValue: 'La Libertad'},
        {value: 'Lambayeque', displayValue: 'Lambayeque'},
        {value: 'Lima', displayValue: 'Lima'},
        {value: 'Loreto', displayValue: 'Loreto'},
        {value: 'Madre de Dios', displayValue: 'Madre de Dios'},
        {value: 'Moquegua', displayValue: 'Moquegua'},
        {value: 'Pasco', displayValue: 'Pasco'},
        {value: 'Piura', displayValue: 'Piura'},
        {value: 'Puno', displayValue: 'Puno'},
        {value: 'San Martin', displayValue: 'San Martin'},
        {value: 'Tacna', displayValue: 'Tacna'},
        {value: 'Tumbes', displayValue: 'Tumbes'},
        {value: 'Ucayali', displayValue: 'Ucayali'}
      ]
    },
    elementClasses: ['Half'],
    value: 'Lima',
    validation: {},
    valid: true,
    label: 'Departamento',
    errorMessage: ''
  },
  province: { 
    elementType: 'input', 
    elementConfig: {
      type: 'text',
      autoComplete: 'shipping locality'
    },
    elementClasses: ['Half'],
    value: '',
    validation: {
      required: true
    },
    valid: false,
    touched: false,
    label: 'Provincia',
    errorMessage: ''
  },
  district: { 
    elementType: 'input', 
    elementConfig: {
      type: 'text'
    },
    elementClasses: ['Half'],
    value: '',
    validation: {
      required: true
    },
    valid: false,
    touched: false,
    label: 'Distrito',
    errorMessage: ''
  },
  address: { 
    elementType: 'input', 
    elementConfig: {
      type: 'text',
      autoComplete: 'shipping street-address'
    },
    elementClasses: ['Half'],
    value: '',
    validation: {
      required: true
    },
    valid: false,
    touched: false,
    label: 'Dirección',
    errorMessage: ''
  },
  phone: { 
    elementType: 'input', 
    elementConfig: {
      type: 'tel',
      autoComplete: 'tel'
    },
    elementClasses: ['Half'],
    value: '',
    validation: {
      required: true
    },
    valid: false,
    touched: false,
    label: 'Teléfono',
    errorMessage: ''
  },
  email: { 
    elementType: 'input', 
    elementConfig: {
      type: 'email',
      autoComplete: 'username'
    },
    value: '',
    validation: {
      required: true
    },
    valid: false,
    touched: false,
    label: 'Correo'
  },
  password: { 
    elementType: 'input', 
    elementConfig: {
      type: 'password',
      autoComplete: 'new-password'
    },
    value: '',
    validation: {
      required: true
    },
    valid: false,
    touched: false,
    label: 'Contraseña'
  },
  passwordConfirm: { 
    elementType: 'input', 
    elementConfig: {
      type: 'password',
      autoComplete: 'new-password'
    },
    value: '',
    validation: {
      required: true,
      equals: 'password'
    },
    valid: false,
    touched: false,
    label: 'Repetir contraseña',
    errorMessage: ''
  },
  termsConditions: {
    elementType: 'checkbox',
    elementConfig: {
      type: 'chekbox',
      name: 'termsConditions',
      checks: [
        {value: 'agree', label: 'Acepto'}
      ]
    },
    value: '',
    validation: {},
    valid: true
  }
}

export default agencyRequest;