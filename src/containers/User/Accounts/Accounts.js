import React, { Component } from 'react';
import { auth, db } from '../../../lib/firebase';

import classes from './Accounts.css';
import Aux from '../../../hoc/Aux/Aux';
import Header from '../../../components/UI/Header/Header';
import Card from '../../../components/UI/Card/Card';
import Form from '../../../components/Form/Form';
import Button from '../../../components/UI/Button/Button';
import Modal from '../../../components/UI/Modal/Modal';
import Panel from '../../../components/UI/Panel/Panel';
import CardButton from '../../../components/UI/CardButton/CardButton';

class Accounts extends Component {
  state = {
    accounts: [],
    accountDetails: {},
    showModal: false,
    modalAction: '',
  }

  componentWillMount() {
    console.log('[componentWillMount]');
    const user = auth.currentUser;

    const accountsRef = db.ref(`/users/${user.uid}/accounts`);
    this.fetchAccounts(accountsRef);
  }

  fetchAccounts = ref => {
    let accounts = []
    ref.on('value', snapshot => {
      accounts = []
      snapshot.forEach(snap => {
        accounts = [
          ...accounts,
          {
            ...snap.val(),
            id: snap.key
          }
        ]
      })
      
      this.setState({accounts: accounts})
    })
  }

  showModalHandler = (action = '') => {
    this.setState({showModal: !this.state.showModal, modalAction: action});
  }

  accountDetailsHandler = (id, action) => {
    this.showModalHandler(action);
    this.state.accounts.map(account => {
      if (account.id === id) {
        this.setState({accountDetails: account});
      }
      return true;
    })
    console.log('ejecutado');
  }

  modalContentHandler = () => {
    let content = { 
      title: 'Ups!',
      body: <p>Nada que ver aqui</p>
    }

    if (this.state.showModal) {
      const action = this.state.modalAction;
      if (action === 'add') {
        return content = { 
          title: 'Agregar cuenta',
          body: <Aux><p>Registra la cuenta donde las agencias enviarán su cambio</p><Form type="AddAccount" submitButton="Agregar" /></Aux>
        }
      }
  
      if (action === 'edit') {
        return content = { 
          title: 'Editar cuenta',
          body: <Form type="AddAccount" submitButton="Editar" values={this.state.accountDetails} />
        }
      }
  
      if (action === 'delete') {
        return content = { 
          title: 'Eliminar cuenta',
          body: <Aux><p>Seguro que deseas eliminar esta cuenta?</p><Button btnType="Success" clicked={() => this.deleteAccountHandler(this.state.accountDetails.id)}>Eliminar</Button></Aux>
        }
      }
    }

    return content
  }

  deleteAccountHandler = id => {
    const user = auth.currentUser;
    const accountsRef = db.ref(`/users/${user.uid}/accounts`);
    const updates = {};
    updates[`${user.uid}/accounts/${id}`] = null
    db.ref('users').update(updates).then(() => {
      console.log('[deleteAccountHandler] Deleted');
      this.fetchAccounts(accountsRef);
    }).catch(error => {
      console.log('[deleteAccountHandler] Error', error);
    });

    this.showModalHandler();
  }

  render () {
    let panel = null;
    let cards = null;
    if (this.state.accounts.length === 0) {
      panel = <Panel>No has agregado tus cuentas</Panel>
    } else {
      
      cards = (
        this.state.accounts.map(account => {
          const button = (
            <Aux>
              <CardButton multi clicked={() => this.accountDetailsHandler(account.id, 'delete')}>Eliminar</CardButton>
              <CardButton multi clicked={() => this.accountDetailsHandler(account.id, 'edit')}>Editar</CardButton>
            </Aux>
          )
          return (
            <Card 
              key={account.id} 
              header={account.bank}
              status={account.bank}
              button={button}>
              <p>Moneda: {account.currency}</p>
              <p>Numero de cuenta: {account.number}</p>
              <p>Titular: {account.owner}</p>
            </Card>
          )

        })
      )
    }

    const modalContent = this.modalContentHandler();

    return (
      <Aux>
        <Modal
          show={this.state.showModal} 
          title={modalContent.title}
          modalClosed={this.showModalHandler}>
          {modalContent.body}
        </Modal>
        <Header>Mis cuentas</Header>
        <div className={classes.Accounts}>
          {panel}
          {cards}
          <Button
            btnType="Success"
            clicked={() => this.showModalHandler('add')}>AGREGAR CUENTA</Button>
        </div>
      </Aux>
    );
  }
}

export default Accounts;