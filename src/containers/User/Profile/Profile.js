import React, { Component } from 'react';
import { auth, db } from '../../../lib/firebase';

import classes from './Profile.css';
import Aux from '../../../hoc/Aux/Aux';
import Header from '../../../components/UI/Header/Header';
import CircleImage from '../../../components/UI/CircleImage/CircleImage';
import Placeholder from '../../../assets/images/placeholder.png';
import ProfileSection from './ProfileSection/ProfileSection';
import Form from '../../../components/Form/Form';
import SmallCard from '../../../components/SmallCard/SmallCard';

class Profile extends Component {
  state = {
    userData: {},
    loading: true
  }

  componentWillMount() {
    const user = auth.currentUser;

    db.ref('users').child(user.uid).on('value', snap => {
      this.setState({userData: snap.val(), loading: false})
    })
  }

  render () {
    let generalDescription = null;
    let myAccounts = null;
    if (!this.state.loading) {
      const personalData = this.state.userData.personalData;
      if (personalData.userType === 'natural') {
        generalDescription = (
          <Aux>
            <div>
              <h4>Nombre y apellido</h4>
              <p>{personalData.name} {personalData.lastName}</p>
              <h4>{personalData.docType}</h4>
              <p>{personalData.document}</p>
              <h4>Correo electrónico</h4>
              <p>{personalData.email}</p>
              <h4>Fecha de nacimiento</h4>
              <p>{personalData.birthDate}</p>
            </div>
            <div>
              <h4>País</h4>
              <p>{personalData.country}</p>
              <h4>Departamento</h4>
              <p>{personalData.department}</p>
              <h4>Provincia</h4>
              <p>{personalData.province}</p>
              <h4>Distrito</h4>
              <p>{personalData.district}</p>
              <h4>Dirección</h4>
              <p>{personalData.address}</p>
            </div>
          </Aux>
        )
      } else {
        generalDescription = (
          <Aux>
            <div>
              <h4>Nombre y apellido</h4>
              <p>{personalData.name} {personalData.lastName}</p>
              <h4>{personalData.docType}</h4>
              <p>{personalData.document}</p>
              <h4>Correo electrónico</h4>
              <p>{personalData.email}</p>
              <h4>Fecha de nacimiento</h4>
              <p>{personalData.birthDate}</p>
            </div>
            <div>
              <h4>Empresa</h4>
              <p>{personalData.company}</p>
              <h4>RUC</h4>
              <p>{personalData.ruc}</p>
            </div>
            <div>
              <h4>País</h4>
              <p>{personalData.country}</p>
              <h4>Departamento</h4>
              <p>{personalData.department}</p>
              <h4>Provincia</h4>
              <p>{personalData.province}</p>
              <h4>Distrito</h4>
              <p>{personalData.district}</p>
              <h4>Dirección</h4>
              <p>{personalData.address}</p>
            </div>
          </Aux>
        )
      }

      let accounts = [];
      for (const key in this.state.userData.accounts) {
        accounts = [
          ...accounts,
          this.state.userData.accounts[key]
        ]
      }

      myAccounts = accounts.map(account => {
        return (
          <SmallCard key={account.number} title={account.bank}>
            {account.name}<br/>
            {account.number}<br/>
            {account.currency}<br/>
            {account.owner}
          </SmallCard>
        )
      });
    }
    
    return(
      <Aux>
        <Header>Perfil</Header>
        <div className={classes.Profile}>
          <CircleImage src={Placeholder} alt="SAVIO user profile image" />
          <ProfileSection title="Descripción general">
            {generalDescription}
          </ProfileSection>

          <ProfileSection title="Mis cuentas" icon="edit">
            {myAccounts}
          </ProfileSection>

          <ProfileSection title="Cambio de contraseña">
            <Form type="ChangePassword" submitButton="Cambiar" />
          </ProfileSection>
        </div>
      </Aux>
    )
  }
}

export default Profile;