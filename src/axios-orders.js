import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://savio-38674.firebaseio.com/'
});

export default instance;