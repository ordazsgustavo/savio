import React from 'react';
import { Link } from 'react-router-dom';

import classes from './AgencyRequest.css';
import Aux from '../../../hoc/Aux/Aux';
import Header from '../../../components/UI/Header/Header';
import Panel from '../../../components/UI/Panel/Panel';

import Form from '../../../components/Form/Form';

const AgencyRequest = () => (
  <Aux>
    <Header>Solicitud de registro</Header>
    <div className={classes.AgencyRequest}>
      <Panel centered>
        <Form type="AgencyRequest" submitButton="Solicitar registro" />

        <p>¿Ya estas registrado? <span className={classes.RegisterLink}><Link to="/login">Ingresar</Link></span></p>
      </Panel>
    </div>
  </Aux>
)

export default AgencyRequest;