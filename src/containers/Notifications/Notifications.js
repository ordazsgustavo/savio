import React, { Component } from 'react';
import { auth, db } from '../../lib/firebase';

import classes from './Notifications.css';
import Aux from '../../hoc/Aux/Aux';
import Header from '../../components/UI/Header/Header';
import Panel from '../../components/UI/Panel/Panel';
import NotificationItem from '../../components/Notifications/NotificationItem/NotificationItem';


class Notifications extends Component {
  state = {
    loading: true,
    globalNotifications: [],
    generalNotifications: [],
    notifications: []
  }

  componentWillMount () {
    auth.onAuthStateChanged(user => {
      if (user) {
        const usersRef = db.ref('users')
        const agencysRef = db.ref('agencys')
        const notificationsRef = db.ref('notifications')

        usersRef.child(user.uid).once('value', snapshot => {
          if (snapshot.val()) {
            notificationsRef.child(`users/${user.uid}`).on('value', notiSnap => {
              if (notiSnap.val()) {
                this.fillNotificationsArray(notiSnap)
              }
            })

            notificationsRef.child('users/general').on('value', genSnap => {
              if (genSnap.val()) {
                this.fillNotificationsArray(genSnap, 'general')
              }
            })
          }
        })
        
        agencysRef.child(user.uid).once('value', snapshot => {
          if (snapshot.val()) {
            notificationsRef.child(`agencys/${user.uid}`).on('value', notiSnap => {
              if (notiSnap.val()) {
                this.fillNotificationsArray(notiSnap)
                
              }
            })
  
            notificationsRef.child('agencys/general').on('value', genSnap => {
              if (genSnap.val()) {
                this.fillNotificationsArray(genSnap, 'general')
              }
            })
          }
        })

        notificationsRef.child('global').on('value', snapshot => {
          if (snapshot.val()) {
            this.fillNotificationsArray(snapshot, 'global')
          }
        })

      }
    })
  }

  fillNotificationsArray = (snapshot, notiType) => {
    if (notiType === 'global') {
      let globalNotifications = [];
      snapshot.forEach(snap => {
        globalNotifications = [
          {
            ...snap.val(),
            id: snap.key
          },
          ...globalNotifications
        ]
      })

      this.setState({loading: false, globalNotifications: globalNotifications});
      return;
    } 

    if (notiType === 'general') {
      let generalNotifications = []
      snapshot.forEach(snap => {
        generalNotifications = [
          {
            ...snap.val(),
            id: snap.key
          },
          ...generalNotifications
        ]
      })
  
      this.setState({loading: false, generalNotifications: generalNotifications});
      return;
    }

    let notifications = [];
    snapshot.forEach(snap => {
      notifications = [
        {
          ...snap.val(),
          id: snap.key
        },
        ...notifications
      ]
    })

    this.setState({loading: false, notifications: notifications});
  }

  render () {
    let notifications = null;

    if (this.state.notifications.length === 0) {

      notifications = <Panel>No hay notificaciones</Panel>

    } else {
      
      notifications = (
        <Panel>
          <h3>Notificaciones de pedidos</h3>
          {this.state.notifications.map(noti => (
            <NotificationItem key={noti.generated} icon="dollar-sign" linkTo={noti.linkTo} content={noti.content} since={noti.generated} />
          ))}
        </Panel>
      )
    }

    let generalNotifications = null;

    if (this.state.generalNotifications.length === 0) {

      generalNotifications = <Panel>No hay notificaciones generales</Panel>

    } else {
      
      generalNotifications = (
        <Panel>
          <h3>Generales</h3>
          {this.state.generalNotifications.map(noti => (
            <NotificationItem key={noti.generated} icon="dollar-sign" linkTo={noti.linkTo} content={noti.content} since={noti.generated} />
          ))}
        </Panel>
      )
    }

    let globalNotifications = null;

    if (this.state.globalNotifications.length === 0) {

      globalNotifications = <Panel>No hay anuncios</Panel>

    } else {
      
      globalNotifications = (
        <Panel>
          <h3>Anuncios</h3>
          {this.state.globalNotifications.map(noti => (
            <NotificationItem key={noti.generated} icon="dollar-sign" linkTo={noti.linkTo} content={noti.content} since={noti.generated} />
          ))}
        </Panel>
      )
    }

    return(
      <Aux>
        <Header>Tus notificaciones</Header>
        <div className={classes.Notifications}>
          {notifications}
          {generalNotifications}
          {globalNotifications}
        </div>
      </Aux>
    )
  }
}

export default Notifications;