const Login = {
  email: { 
    elementType: 'input', 
    elementConfig: {
      type: 'email',
      autoComplete: 'username'
    },
    value: '',
    validation: {
      required: true,
      type: 'email'
    },
    errorMessage: '',
    valid: false,
    touched: false,
    label: 'Correo'
  },
  password: { 
    elementType: 'input', 
    elementConfig: {
      type: 'password',
      autoComplete: 'current-password'
    },
    value: '',
    validation: {
      required: true
    },
    valid: false,
    touched: false,
    label: 'Contraseña'
  }
}

export default Login