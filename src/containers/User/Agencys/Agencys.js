import React, { Component } from 'react';
import { db } from '../../../lib/firebase';

import classes from './Agencys.css';
import Aux from '../../../hoc/Aux/Aux';
import Spinner from '../../../components/UI/Spinner/Spinner';
import Header from '../../../components/UI/Header/Header';
import Card from '../../../components/UI/Card/Card';
import StarRating from '../../../components/UI/StarRating/StarRating';

class Agencys extends Component {
  state = {
    agencys: [],
    loading: true
  }

  componentWillMount() {
    const agencysRef = db.ref('/agencys');
    agencysRef.on('value', snap => {
      let agencys = [];
      snap.forEach(snap => {
        agencys = [
          {
            ...snap.val(),
            id: snap.key
          },
          ...agencys
        ]
      });

      this.setState({ loading: false, agencys: agencys });
    })
  }

  listAgencys = () => {
    return this.state.agencys.map(agency => { 
      const starTotal = 5;
      const starPercentage = (agency.rating / starTotal) * 100;
      return ( 
        <Card
          key={agency.id}
          header={agency.data.corporateName}
          image="placeholder"
          tag={{type:'Warning', content:'NEW'}}>
          <p><strong>RUC</strong>: {agency.data.ruc}</p>
          <p><strong>Razón social</strong>: {agency.data.corporateName}</p>
          <br/>
          <p><strong>Puntuación</strong>:</p>
          <StarRating percentage={starPercentage} rating={agency.rating} />
        </Card>
      )
    })
  }

  render() {
    let agencys;
    if (this.state.loading) {
      agencys = <Spinner />
    } else {
      agencys = this.listAgencys()
    }
    return (
      <Aux>
        <Header>Agencias</Header>
        <div className={classes.Agencys}>
          {agencys}
        </div>
      </Aux>
    )
  }
}

export default Agencys;