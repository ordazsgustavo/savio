import React, { Component } from 'react';

import classes from './Layout.css';
import Aux from '../Aux/Aux';
import Toolbar from '../../components/Navigation/Toolbar/Toolbar';
import SideDrawer from '../../components/Navigation/SideDrawer/SideDrawer';
import BackToTop from '../../components/UI/BackToTop/BackToTop';
import Footer from '../../components/Footer/Footer';

class Layout extends Component {
  state = {
    showSideDrawer: false,
    showButton: false
  }

  sideDrawerClosedHandler = () => {
    this.setState({showSideDrawer: false});
  }

  sideDrawerToggleHandler = () => {
    this.setState((prevState) => {
      return {showSideDrawer: !prevState.showSideDrawer};
    });
  }

  showButtonHandler = () => {
    window.onscroll = () => {
      if (window.scrollY > 550) {
        this.setState({showButton: true})
        return;
      }
      this.setState({showButton: false})
    }
  }

  

  render () {
    this.showButtonHandler()

    let scrollButton = null

    if (this.state.showButton) {
      scrollButton = <BackToTop />
    }
    
    return (
      <Aux>
        <Toolbar drawerToggleClicked={this.sideDrawerToggleHandler} />
        <SideDrawer 
          open={this.state.showSideDrawer} 
          closed={this.sideDrawerClosedHandler}
          navItemsClicked={this.sideDrawerToggleHandler} />
        <main className={classes.Content}>
          {this.props.children}
          {scrollButton}
        </main>
        <Footer />
      </Aux>
    );
  }
}

export default Layout;