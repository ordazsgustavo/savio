import React from 'react';

import classes from './DialogContent.css';
import StepperBullet from '../../UI/StepperBullet/StepperBullet';

const dialogContent = (props) => (
  <div className={classes.DialogContent}>
    <p>{props.answer}</p>
    <div className={classes.Inputs}>
      {props.children}
    </div>
    <StepperBullet currentStep={props.currentStep} totalSteps={props.totalSteps} />
  </div>
)

export default dialogContent;