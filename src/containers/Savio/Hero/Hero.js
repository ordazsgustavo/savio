import React from 'react';

import classes from './Hero.css';
import Dialog from '../../../components/Dialog/Dialog';

const hero = (props) => {
  return (
    <div className={classes.Hero}>
      <h2 className={classes.Title}>{props.title}</h2>
      <h3 className={classes.Subtitle}>{props.subtitle}</h3>
      <Dialog {...props} />
    </div>
  );
}
export default hero;