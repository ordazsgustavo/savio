import React from 'react';
import { Link } from 'react-router-dom';

import classes from './CardButton.css';

const cardButton = (props) => {
  let styles = [classes.CardButton];

  if (props.multi) {
    styles = [...styles, classes.Multi];
  }

  let button = null;
  if (props.linkTo) {
    styles = [...styles, classes.Link];
    button = <Link className={styles.join(' ')} to={props.linkTo}>{props.children}</Link>;
  } else {
    button = <button onClick={props.clicked} className={styles.join(' ')}>{props.children}</button>;
  }

  return button;
}

export default cardButton;