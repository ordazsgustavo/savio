import React, { Component } from 'react';

import classes from './Notifications.css';
import NotificationItem from './NotificationItem/NotificationItem';

class Notifications extends Component {
  render () {
    return(
      <div className={classes.Notifications} style={{display: this.props.display ? 'block' : 'none'}}>
        <div className={classes.Header}>
          Notificaciones
        </div>
        <div>
          <NotificationItem linkTo="/home" />
          <NotificationItem linkTo="/home" />
        </div>
      </div>
    )
  }
}

export default Notifications;