import React, { Component } from 'react';

// import Chartist from '../../assets/js/chartist/chartist';

class Charts extends Component {
  componentDidMount () {
    this.updateChart(this.props.data, this.props.options, this.props.responsiveOptions);
  }
  updateChart = (data, options, responsiveOptions) => {
    return new window.Chartist.Line('.chart', data, options, responsiveOptions);
  }
  render () {
    return (
      <div className="chart" style={{display:'inline-block', width:'100%', height:'100%'}}></div>
    )
  }
}

export default Charts;