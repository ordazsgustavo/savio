import React from 'react';
import { auth } from '../../../lib/firebase';
import Spinner from '../../../components/UI/Spinner/Spinner';

const logout = (props) => {

  auth.signOut().then(() => {
    window.location.assign('/');
  }).catch(error => {
    // An error happened.
    console.log(error);
  });
  

  return (
    <Spinner />
  );
}

export default logout;