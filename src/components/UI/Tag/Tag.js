import React from 'react';

import classes from './Tag.css';

const tag = (props) => {
  let styles = [classes.Tag]

  if (props.type) {
    styles = [...styles, classes[props.type]]
  }
  return (
    <div className={styles.join(' ')}>{props.children}</div>
  )
}

export default tag;