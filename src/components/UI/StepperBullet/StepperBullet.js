import React from 'react';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';

import classes from './StepperBullet.css';

const stepperBullet = (props) => {
  let styles = [classes.Steps];
  if (props.inverse) {
    styles = [
      ...styles,
      classes.Inverse
    ]
  }
  const totalSteps = [];
  for (let i = 0; i < props.totalSteps; i++) {
    totalSteps[i] = <FontAwesomeIcon key={i} icon={['far', 'circle']} />
  }

  const currentStep = [];
  for (let i = 0; i < props.currentStep+1; i++) {
    currentStep[i] = <FontAwesomeIcon key={i} icon="circle" />
  }
  return (
    <div className={styles.join(' ')}>
      {totalSteps}
      <div className={classes.StepsInner}>
        {currentStep}
      </div>
    </div>
  )
}

export default stepperBullet;